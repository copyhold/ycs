(function($) {
Drupal.behaviors.wrapThis = {
attach: function (context, settings) {
$(".views-exposed-form .views-exposed-widget:not(.views-widget-filter-secondary,.views-submit-button,.views-reset-button)" , context).wrapAll("<div class='views-widget-filter-primary'></div>");

$("#edit-stockid-wrapper, #edit-weight-wrapper, #edit-ppc-wrapper, #edit-price-wrapper, #edit-title-wrapper, #edit-field-diamond-weight-value-wrapper, #edit-commerce-price-amount-wrapper, .exposed-form-buttons-wrapper" , context).wrapAll("<div class='wrapperright'></div>");
																																																																																																																					
$("#edit-shape-wrapper, #edit-color-wrapper, #edit-clarity-wrapper, #edit-fancy-color-wrapper, #edit-field-diamond-fancy-color-value-wrapper, #edit-field-diamond-clarity-value-wrapper, #edit-field-diamond-color-value-wrapper, #edit-field-diamond-clarity-value-wrapper" , context).wrapAll("<div class='wrapperleft'></div>");
}
};
})(jQuery);	

(function($) {
Drupal.behaviors.diamondSearch = {
attach: function (context, settings) {

if (navigator.userAgent.indexOf('Mac OS X') != -1) {
    $("body").addClass("mac");
} else {
    $("body").addClass("pc");
}

$("#edit-secondary").removeClass("collapsed");
	
$("label[for='edit-weight']").appendTo("#edit-weight-wrapper .views-widget");
$(".form-item-weight-max").appendTo("#edit-weight-wrapper .views-widget");
$(".form-item-weight-min").appendTo("#edit-weight-wrapper .views-widget");
$("label[for='edit-weight-max']").text('to');

$("label[for='edit-ppc']").appendTo("#edit-ppc-wrapper .views-widget");
$(".form-item-ppc-max").appendTo("#edit-ppc-wrapper .views-widget");
$(".form-item-ppc-min").appendTo("#edit-ppc-wrapper .views-widget");
$("label[for='edit-ppc']").text('PPC');
$("label[for='edit-ppc-max']").text('to');

$("label[for='edit-price']").appendTo("#edit-price-wrapper .views-widget");
$("label[for='edit-price-max']").appendTo("#edit-price-wrapper .views-widget");
$(".form-item-price-max").appendTo("#edit-price-wrapper .views-widget");
$(".form-item-price-min").appendTo("#edit-price-wrapper .views-widget");
$("label[for='edit-price-max']").text('to');
$("label[for='edit-price-max']").appendTo(".form-item-price-min");

//$("body.page-node .form-type-select").appendTo("body.page-node form.commerce-add-to-cart");

//$("body.page-node form.commerce-add-to-cart div:first-child").appendTo("body.page-node form.commerce-add-to-cart");

$("body.front #node-webform-463").appendTo("#homefooter .homefooter-wrapper");


$("#popup-active-overlay").appendTo("#page");
//White
$('.form-item-edit-shape-round').attr('title' , 'Round');
$('.form-item-edit-shape-emerald').attr('title' , 'Emerald');
$('.form-item-edit-shape-marquise').attr('title' , 'Marquise');
$('.form-item-edit-shape-princess').attr('title' , 'Princess');
$('.form-item-edit-shape-oval').attr('title' , 'Oval');
$('.form-item-edit-shape-pear').attr('title' , 'Pear');
$('.form-item-edit-shape-asscher').attr('title' , 'Asscher');
$('.form-item-edit-shape-radiant').attr('title' , 'Radiant');
$('.form-item-edit-shape-heart').attr('title' , 'Heart');
$('.form-item-edit-shape-cushion').attr('title' , 'Cushion');
//Fancy
$('.form-item-edit-fancy-color-yellow').attr('title', 'Yellow Diamonds');
$('.form-item-edit-fancy-color-pink').attr('title' , 'Pink Diamonds');
$('.form-item-edit-fancy-color-blue').attr('title' , 'Blue Diamonds');
$('.form-item-edit-fancy-color-gray').attr('title' , 'Gray Diamonds');
$('.form-item-edit-fancy-color-red').attr('title' , 'Red Diamonds');
$('.form-item-edit-fancy-color-orange').attr('title' , 'Orange Diamonds');
$('.form-item-edit-fancy-color-green').attr('title' , 'Greeen Diamonds');
$('.form-item-edit-fancy-color-brown').attr('title' , 'Brown Diamonds');
$('.form-item-edit-fancy-color-chameleon').attr('title' , 'Chameleon Diamonds');
$('.form-item-edit-fancy-color-white').attr('title' , 'White Diamonds');
}
};
})(jQuery);

