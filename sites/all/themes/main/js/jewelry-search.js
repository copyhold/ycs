// JavaScript Document
(function($) {
Drupal.behaviors.wrapJewelry = {
attach: function (context, settings) {
$(".views-exposed-form .views-exposed-widget:not(.views-widget-filter-secondary,.views-submit-button,.views-reset-button)" , context).wrapAll("<div class='views-widget-filter-primary'></div>");

$("#edit-stockid-wrapper, #edit-weight-wrapper, #edit-ppc-wrapper, #edit-price-wrapper, #edit-title-wrapper, #edit-field-diamond-weight-value-wrapper, #edit-commerce-price-amount-wrapper, .exposed-form-buttons-wrapper" , context).wrapAll("<div class='wrapperright'></div>");
																																																																																																																					
$("#edit-field-jewelry-metal-value-wrapper, #edit-sku-wrapper, .views-widget-sort-by, .views-widget-sort-order, #edit-parcel-shape-wrapper, #edit-parcel-color-wrapper, #edit-parcel-clarity-wrapper, #edit-parcel-quantity-wrapper, body.page-parcels #edit-price-wrapper" , context).wrapAll("<div class='wrapperleft'></div>");

$("label[for='edit-commerce-price-amount']").appendTo("#edit-commerce-price-amount-wrapper .views-widget");
$(".form-item-commerce-price-amount-max").appendTo("#edit-commerce-price-amount-wrapper .views-widget");
$(".form-item-commerce-price-amount-min").appendTo("#edit-commerce-price-amount-wrapper .views-widget");
$("label[for='edit-commerce-price-amount-max']").text('to');

$(".form-item-parcel-quantity-max").appendTo("#edit-parcel-quantity-wrapper .views-widget");
$(".form-item-parcel-quantity-min").appendTo("#edit-parcel-quantity-wrapper .views-widget");
$("label[for='edit-parcel-quantity-max']").text('to');
$("label[for='edit-parcel-quantity-max']").appendTo('.form-item-parcel-quantity-min');

$("label[for='edit-price']").appendTo("#edit-price-wrapper .views-widget");
$("label[for='edit-price-max']").appendTo("#edit-price-wrapper .views-widget");
$(".form-item-price-max").appendTo("#edit-price-wrapper .views-widget");
$(".form-item-price-min").appendTo("#edit-price-wrapper .views-widget");
$("label[for='edit-price-max']").text('to');
$("label[for='edit-price-max']").appendTo(".form-item-price-min");
}
};
})(jQuery);	