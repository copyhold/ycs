(function($) {
Drupal.behaviors.addJewelrySlider = {
	
  attach: function (context, settings) {

  //Price
  
   var minprice = $('#edit-commerce-price-amount-min');

   var maxprice = $('#edit-commerce-price-amount-max');

   // Set default values or use those passed into the form

   var init_min_price = ('' == minprice.val()) ? 100 : minprice.val();

   var init_max_price = ('' == maxprice.val()) ? 50000 : maxprice.val();

   // Set initial values of the slider

   minprice.val(init_min_price);

   maxprice.val(init_max_price);


//Price

if($('#price-slider').length==0){

 $('#edit-commerce-price-amount-min').parents('div.form-item-commerce-price-amount-min').before(

        $('<div id="price-slider"></div>').slider({

        range: true,

        min: 100,     // Adjust slider min and max to the range

        max: 50000,    // of the exposed filter.

		step: 200,

        values: [init_min_price ,init_max_price],

        slide: function(event, ui){

          // Update the form input elements with the new values when the slider moves

         minprice.val(ui.values[0]);

         maxprice.val(ui.values[1]);

		  }

      

      })

    );     

}



//ppc slider key events	

	minprice.keyup(function() {

	$("#price-slider").slider("values" , 0, parseInt($(this).val()));

	});
	
		
	maxprice.keyup(function() {

	$("#price-slider").slider("values" , 1, parseInt($(this).val()));

		
		});
//ppc key events	

	 }
  };
})(jQuery);





