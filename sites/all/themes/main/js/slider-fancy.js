(function($) {
		  
Drupal.behaviors.addSlider = {
	
  attach: function (context, settings) {

  //weight variables

   var minweight = $('#edit-weight-min');

   var maxweight = $('#edit-weight-max');

   // Set default values or use those passed into the form

   var init_min_weight = ('' == minweight.val()) ? 0.1 : minweight.val();

   var init_max_weight = ('' == maxweight.val()) ? 10 : maxweight.val();

   // Set initial values of the slider

   minweight.val(init_min_weight);

   maxweight.val(init_max_weight);

  //Price
  
   var minprice = $('#edit-price-min');

   var maxprice = $('#edit-price-max');

   // Set default values or use those passed into the form

   var init_min_price = ('' == minprice.val()) ? 100 : minprice.val();

   var init_max_price = ('' == maxprice.val()) ? 50000 : maxprice.val();

   // Set initial values of the slider

   minprice.val(init_min_price);

   maxprice.val(init_max_price);

//Weight

if($('#weight-slider').length==0){

 $('#edit-weight-min').parents('div.form-item-weight-min').before(

        $('<div id="weight-slider"></div>').slider({

        range: true,

        min: 0.10,     // Adjust slider min and max to the range

        max: 10.00,    // of the exposed filter.

		step: 0.10,

        values: [init_min_weight ,init_max_weight],

        slide: function(event, ui){

          // Update the form input elements with the new values when the slider moves

         minweight.val(ui.values[0]);

         maxweight.val(ui.values[1]);

		  }

      

      })

    );     

}

//Price

if($('#price-slider').length==0){

 $('#edit-price-min').parents('div.form-item-price-min').before(

        $('<div id="price-slider"></div>').slider({

        range: true,

        min: 100,     // Adjust slider min and max to the range

        max: 50000,    // of the exposed filter.

		step: 200,

        values: [init_min_price ,init_max_price],

        slide: function(event, ui){

          // Update the form input elements with the new values when the slider moves

         minprice.val(ui.values[0]);

         maxprice.val(ui.values[1]);

		  }

      

      })

    );     

}


//weight slider	key events

	minweight.keyup(function() {

	 $("#weight-slider").slider("values" , 0, parseFloat($(this).val()));

		

		 });

	maxweight.keyup(function() {

	$("#weight-slider").slider("values" , 1, parseFloat($(this).val()));

		
		});

//Price slider key events	

	minprice.keyup(function() {

	$("#price-slider").slider("values" , 0, parseInt($(this).val()));

	});
	
		
	maxprice.keyup(function() {

	$("#price-slider").slider("values" , 1, parseInt($(this).val()));

		
		});
//Price key events	

	 }
  };
})(jQuery);





