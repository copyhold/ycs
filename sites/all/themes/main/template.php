<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 * 
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
 function main_preprocess_node(&$variables) {
  //print_r(array_keys($variables)) ; return;
 if ($variables['type'] == 'diamond' || $variables['type'] == 'jewelry_setting'){
  drupal_add_js(path_to_theme() .'/js/diamond.js', 'file'); 
  drupal_add_js(path_to_theme() .'/js/jquery.printElement.js', 'file');
  drupal_add_js(path_to_theme() .'/js/printCert.js', 'file');
 
  }
  }
 /**
 * Preprocess page
 */

function main_preprocess_page(&$variables) {
//Change logo for reseller
 global $user;
 if(in_array('reseller' , $user->roles)){
 $user_fields = user_load($user->uid);
 if(isset($user_fields->field_profile_logo['und'][0]['uri'])){
 $logo_path = file_create_url($user_fields->field_profile_logo['und'][0]['uri']);
 $variables['logo'] = $logo_path;
  }
 }
//Set title to user login page
if(arg(0)=='user'){
//drupal_set_title('Login or Create an Account');
 $variables['title'] = 'Login or Create an Account';
} 
//Add js for diamond search and jewelry 
 if(arg(0)=='diamond-search'){
 drupal_add_library('system' , 'ui.slider');
 drupal_add_js(path_to_theme() . '/js/slider-white.js' , 'file');
 drupal_add_js(path_to_theme() .'/js/diamond-search.js', 'file');
 }
 elseif(arg(0)=='fancy-diamond-search'){
 drupal_add_library('system' , 'ui.slider');
 drupal_add_js(path_to_theme() . '/js/slider-fancy.js' , 'file');
 drupal_add_js(path_to_theme() .'/js/diamond-search.js', 'file');
 }
 elseif(arg(0)=='jewelry-showcase'){
 drupal_add_library('system' , 'ui.slider');
 drupal_add_js(path_to_theme() . '/js/slider-jewelry.js' , 'file');
 drupal_add_js(path_to_theme() . '/js/jewelry-search.js', 'file');
 } 
elseif(arg(0)=='parcels'){
 drupal_add_js(path_to_theme() . '/js/jewelry-search.js', 'file');
}
elseif(arg(0)=='user'){
drupal_add_js(path_to_theme() . '/js/user-register.js', 'file');
}
elseif((arg(3)=='discounts' && arg(4)=='manage') || (arg(2)=='store' && arg(3)=='discounts')){
	 if(in_array('reseller' , $user->roles)){
		 drupal_add_css(path_to_theme() . '/css/reseller.css' , 'file');
	 }
  }
}
 
 /**
 * Put exposed filters into collapsible fieldsets.
 */
//function main_form_alter(&$form, &$form_state, $form_id) {
////White
//if($form['#id']=='views-exposed-form-diamond-search-page'){
//$form['field_diamond_weight_value']['#prefix'] = '<fieldset id="fieldset-weight" class="collapsible ">
//<legend><span class="fieldset-legend">Carat Weight</span></legend>
//<div class="fieldset-wrapper">';
//$form['field_diamond_weight_value']['#suffix'] = '</fieldset>';
////PPC
//$form['field_diamond_ppc_value']['#prefix'] = '<fieldset id="fieldset-weight" class="collapsible ">
//<legend><span class="fieldset-legend">PPC</span></legend>
//<div class="fieldset-wrapper">';
//$form['field_diamond_ppc_value']['#suffix'] = '</fieldset>';
////Stock ID
//$form['title_field_value']['#prefix'] = '<fieldset id="fieldset-weight" class="collapsible collapsed ">
//<legend><span class="fieldset-legend">Stock ID</span></legend>
//<div class="fieldset-wrapper">';
//$form['title-field-value']['#suffix'] = '</fieldset>';
// }
////Fancy color
//if($form['#id']=='views-exposed-form-fancy-diamonds-page'){
// //Weight
//$form['weight']['#prefix'] = '<fieldset id="fieldset-weight" class="collapsible ">
//<legend><span class="fieldset-legend">Carat Weight</span></legend>
//<div class="fieldset-wrapper">';
//$form['weight']['#suffix'] = '</fieldset>';
////Price
//$form['price']['#prefix'] = '<fieldset id="fieldset-price" class="collapsible ">
//<legend><span class="fieldset-legend">Total Price</span></legend>
//<div class="fieldset-wrapper">';
//$form['price']['#suffix'] = '</fieldset>';
////Stock ID
//$form['title_field_value']['#prefix'] = '<fieldset id="fieldset-weight" class="collapsible collapsed ">
//<legend><span class="fieldset-legend">Stock ID</span></legend>
//<div class="fieldset-wrapper">';
//$form['title-field-value']['#suffix'] = '</fieldset>';
// }
//}
