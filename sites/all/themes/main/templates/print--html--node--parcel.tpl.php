<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['css']; ?>
  </head>
  <body>
  <!--Get Diamond node tid-->   
    <?php 
	$prod_id = $node->field_product['und'][0]['product_id'];
	$product = commerce_product_load($prod_id);
	$amount=$product->commerce_price['und'][0]['amount'];
    $currency_code=$product->commerce_price['und'][0]['currency_code'];
	$price_full=commerce_currency_format($amount, $currency_code);
	$price_split=explode('.' , $price_full); 
	$price=$price_split[0];
	?>
	<div class="print-logo"><?php print $print['logo']; ?></div>
    <p />
    <hr class="print-hr" />
	<h1><?php print $node->title; ?></h1>
	<!--Content-->
    <div class="print-content">
	<?php /*?><?php print $print['content']; ?><?php */?>
	<div>
	<span class="print-label">Stock ID: </span><span class="print-value"><?php print render($product->sku); ?></span>
	</div>
	<!--//Stock ID-->
	<div>
	<span class="print-label">Shape: </span><span class="print-value">
	<?php if(isset($node->field_parcel_shape[$node->language][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_shape['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
	</span>
	</div>
	<!--//Shape-->
	<!--Color-->
	<div>
	<span class="print-label">Color: </span><span class="print-value">
	<?php if(isset($node->field_parcel_color[$node->language][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_color['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
	</span>
	</div>
	<!--//Color-->
	<!--Clarity-->
	<div>
	<span class="print-label">Clarity: </span><span class="print-value">
<?php if(isset($node->field_parcel_clarity[$node->language][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_clarity['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
	</span>
	</div>
	<!--//Clarity-->
	<!--Average Weight-->
	<div>
	<span class="print-label">Average Weight: </span><span class="print-value">
	<?php print render($node->field_parcel_average_weight['und'][0]['value']); ?> Ct.
	</span>
	</div>
	<!--//Average Weight-->
	<!--Total Weight-->
	<div>
	<span class="print-label">Total Weight: </span><span class="print-value">
	<?php print render($node->field_parcel_total_weight['und'][0]['value']); ?> Ct.
	</span>
	</div>
	<!--//Total Weight-->
	<div>
	<span class="print-label">Total Price: </span><span class="print-value"><?php print $price; ?></span>
	</div>
	<!--//Total price-->
	</div>
	<!--//Content-->
    <div class="print-footer"><?php print $print['footer_message']; ?></div>
    <hr class="print-hr" />
    <?php print $print['footer_scripts']; ?>
  </body>
</html>
