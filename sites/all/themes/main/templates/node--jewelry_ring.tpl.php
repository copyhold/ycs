<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
  <footer class="submitted">
    <?php print $submitted; ?>
  </footer>
  <?php endif; ?>
<!--Add jquery ui tabs library-->   
<?php
drupal_add_library ( 'system' , 'ui.tabs' );
?>
<?php
drupal_add_js ( 'jQuery(document).ready(function(){
jQuery("#tabs").tabs();
});
' , 'inline' );
?>
<?php
drupal_add_js ( '

 function showReport(){

  	var tab_offset = jQuery("#tabs").offset().top;

  	jQuery("body,html").animate({scrollTop: tab_offset});

  }
 
   jQuery(function() {

  	var $tabs = jQuery("#tabs").tabs();
	
  	jQuery("#show-cert-jewelry").click(function() {
	   
  		$tabs.tabs({active:3});

	  	showReport();

  	});

  });
' , 'inline' );
?>
<!--//Jquery ui tabs-->
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
	  hide($content['product:title_field']);
	  hide($content['body']);
	  hide($content['field_jewelry_cert_image']);
	  hide($content['field_jewelry_addthis']);
	  hide($content['comments']);
      hide($content['links']);
    //  print render($content);
    ?>
<!--left info wrapper-->
<div class="left-info-wrapper">
<!--Image gallery-->
<div id="diam-diagram-view" class="diamond-diagram-container">
<?php print render($content['product:field_jewelry_image']); ?>
</div>
<!--//Image gallery-->
</div>
<!--Left info wrapper-->
<!--Right info wrapper-->
<div class="right-info-wrapper">
<!--Diamond info-->
<div class="diamond-info">
<h2><?php print render($content['product:title_field']); ?></h2>
<p class="jewelry-body">
<?php print render($content['body']); ?>
</p>
<!--Stock ID-->
<p class="jewelry-ref">
<span class="label">SKU:&nbsp;</span>
<span class="value"><?php print render($content['product:commerce_price']['#object']->sku); ?></span>
</p>
<!--//Stock ID-->
<!--View cert-->
<?php if(!empty($node->field_jewelry_lab['und'][0]['value'])): ?>
<p> <span class="cert-link-label"><?php print t('View certificate') . ':&nbsp;'; ?></span><span id="show-cert-jewelry"><?php print render($node->field_jewelry_lab['und'][0]['value']); ?>&nbsp;<?php print t('Report'); ?></span></p>
<?php endif; ?>
</div>
<!--//View cert-->
<!--//Mainb-->
    <div class="mainb">
        <!--Add-to-cart-->
      <div class="add-to-cart">
     <?php print render($content); ?>
    </div>
        <!--//Add to cart-->
    </div>
<!--//Mainb-->	
<div class="print-back-links">
    <!--Back to search-->
    <div class="back-to-search">
    <a href="/jewelry-showcase/rings">Back to rings</a>
    </div>
    <!--//Back to Search-->
    <!--Email page-->
    <div class="print-mail">
    <?php print print_mail_insert_link(); ?>
    </div>
    <!--//Email page-->
	<!--Print page-->
    <div class="print-page">
    <?php print print_insert_link(); ?>
    </div>
    <!--//Print page-->
    <!--Enquire-->
    <div class="enquire">
    <a href="/enquire?item=<?php print 'Item: ' . $node->title; ?>">Enquire</a>
    </div>
    <!--//Enquire-->
	<!--AddThis-->
	<div class="addthis">
	<?php print render($content['field_jewelry_addthis']); ?>
	</div>
	<!--//AddThis-->
</div>
<!--//Print back links-->
</div>
<!--//Right info wrapper-->
</div>
<!--//Left-right-info wrapper-->
</div>

<!--Tabs-->  
<div id="tabs">
<ul>
<li><a href="#tabs-1">Item Details</a></li>
<li><a href="#tabs-2">Similar items</a></li>
<li><a href="#tabs-3">Recently Viewed</a></li>
<li><a href="#tabs-4">Certificate</a></li>
</ul>
<div id="tabs-1">
<!--Diamond details fields-->
	<div class="diamond-fields">
	<!--Column one-->
	<div class="diam-column-one">
	<!--SKU-->
	<div class="diam-details-row">
	<span class="label">Stock ID:</span>
	<span class="value"><?php print render($content['product:commerce_price']['#object']->sku); ?></span>
	</div>
	<!--//SKU-->
	<!--Metal-->
	<div class="diam-details-row">
	<span class="label">Metal:</span>
	<span class="value"><?php print render($node->field_jewelry_metal['und'][0]['value']); ?></span>
	</div>
	<!--//Metal-->
	<!--Ring style-->
	<div class="diam-details-row">
	<span class="label">Ring Style:</span>
	<span class="value"><?php print render($node->field_jewelry_ring_style['und'][0]['value']); ?></span>
	</div>
	<!--//Ring style-->
	<!--Finger size-->
	<div class="diam-details-row">
	<span class="label">Ring Size:</span>
	<span class="value"><?php print render($node->field_jewelry_finger_size['und'][0]['value']); ?></span>
	</div>
	<!--//Finger size-->
	</div>
	<!--//Column one-->
	<!--Column two-->
	<div class="diam-column-two">
	<!--Diamond shape-->
	<div class="diam-details-row">
	<span class="label">Diamond Shape:</span>
	<span class="value"><?php print render($node->field_jewelry_diamond_shape['und'][0]['value']); ?></span>
	</div>
	<!--//Diamond shape-->
	<!--Center diamond-->
	<div class="diam-details-row">
	<span class="label">Center Diamond:</span>
	<span class="value"><?php print render($node->field_jewelry_center_diamond['und'][0]['value']); ?></span>
	</div>
	<!--//Center diamond-->
	<!--Side diamonds-->
	<div class="diam-details-row">
	<span class="label">Side Diamonds:</span>
	<span class="value"><?php print render($node->field_jewelry_side_diamonds['und'][0]['value']); ?></span>
	</div>
	<!--//Side diamonds-->
	<!--Certificate-->
	<div class="diam-details-row">
	<span class="label">Lab:</span>
	<span class="value"><?php print render($node->field_jewelry_lab['und'][0]['value']); ?></span>
	</div>
	<!--//Certificate-->
    </div>
	<!--//Column two-->
	</div>
	<!--//Diamond fields-->
</div>
<!--//Tabs 1-->
<!--Tabs 2-->
<div id="tabs-2">
<?php print views_embed_view('related_products' , 'block'); ?>
</div>
<!--//Tabs 2-->
<!--Tabs 3-->
<div id="tabs-3">
<?php print views_embed_view('recently_read', 'block_2'); ?>
</div>
<!--//Tabs 3-->
<!--Tabs 4-->
<div id="tabs-4">
<?php if(!empty($node->field_jewelry_certificate['und'][0]['value'])): ?>
<div><span>
<strong>Certificate#</strong>:&nbsp;<?php print render($node->field_jewelry_certificate['und'][0]['value']); ?>
</span>
<br />
</div>
<?php endif; ?>
<div>
<?php print render($content['field_jewelry_cert_image']); ?> 
</div>
</div>
<!--//Tabs 4-->
</div>
<!--//Tabs-->
<div class="clearfix">
  <?php if (!empty($content['links'])): ?>
   <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
  <?php endif; ?>
 <?php print render($content['comments']); ?>
  </div>
</article>
