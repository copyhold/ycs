<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['css']; ?>
  </head>
  <body>
    <?php 
	$prod_id = $node->field_product['und'][0]['product_id'];
	$product = commerce_product_load($prod_id);
    $amount=$product->commerce_price['und'][0]['amount'];
    $currency_code=$product->commerce_price['und'][0]['currency_code'];
	$price_full=commerce_currency_format($amount, $currency_code);
	$price_split=explode('.' , $price_full); 
	$price=$price_split[0];
  ?>
    <?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>
    <div class="print-logo"><?php print $print['logo']; ?></div>
    <p />
    <hr class="print-hr" />
	<h3><?php print render($node->title); ?></h3>
	<div class="print-content">
	<div>
    <?php print render($node->body['und'][0]['value']); ?>
 	</div>
	<!--//Body-->
	<div>
	<span class="print-label">Price: </span><span class="print-value"><?php print $price; ?></span>
	</div>
	<!--//Total price-->
	<div>
	<span class="print-label">Reference no.: </span><span class="print-value"><?php print render($product->sku); ?></span>
	</div>
	<!--//Reference no.-->
	<div>
	<span class="print-label">Metal: </span><span class="print-value"><?php print render($node->field_jewelry_metal[$node->language][0]['value']); ?></span>
	</div>
	<!--//Metal-->
	<div>
	<span class="print-label">Ring style: </span><span class="print-value"><?php print render($node->field_jewelry_ring_style[$node->language][0]['value']); ?></span>
	</div>
	<!--//Ring style-->
	<div>
	<span class="print-label">Finger Size: </span><span class="print-value"><?php print render($node->field_jewelry_finger_size[$node->language][0]['value']); ?></span>
	</div>
	<!--//Finger size-->
	<div>
	<span class="print-label">Diamond Shape: </span><span class="print-value"><?php print render($node->field_jewelry_diamond_shape[$node->language][0]['value']); ?></span>
	</div>
	<!--//Diamond shape-->
	<div>
	<span class="print-label">Center Diamond: </span><span class="print-value"><?php print render($node->field_jewelry_center_diamond[$node->language][0]['value']); ?></span>
	</div>
	<!--//Center diamond-->
	<?php if($node->field_jewelry_side_diamonds[$node->language][0]['value']): ?>
	<div>
	<span class="print-label">Side Diamonds: </span><span class="print-value"><?php print render($node->field_jewelry_side_diamonds[$node->language][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Side diamonds-->
	<div>
	<span class="print-label">Lab: </span><span class="print-value"><?php print render($node->field_jewelry_lab[$node->language][0]['value']); ?></span>
	</div>
	<!--//Lab-->
	<div>
	<br />
	<br />
	<img src="<?php print file_create_url($node->field_jewelry_cert_image['und'][0]['uri']); ?>"  width="800" />
	</div>
	<!--//Cert.-->
	<?php /*?><?php print $print['content']; ?><?php */?>
	</div>
    <div class="print-footer"><?php print $print['footer_message']; ?></div>
    <hr class="print-hr" />
    <?php print $print['footer_scripts']; ?>
  </body>
</html>
