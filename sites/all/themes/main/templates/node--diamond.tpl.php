<article<?php print $attributes; ?>>
<?php print $user_picture; ?>
<?php print render($title_prefix); ?>
<?php if (!$page && $title): ?>
    <header>
        <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    </header>
<?php endif; ?>

<?php print render($title_suffix); ?>
<?php if ($display_submitted): ?>
    <footer class="submitted">
    <?php print $submitted; ?>
    </footer>
<?php endif; ?>
<!--Get Diamond node tid-->
<?php
$tid = $node->field_diamond_category[$node->language][0]['tid'];
?>
<!--Add jquery ui tabs library-->
<?php
drupal_add_library ( 'system' , 'ui.tabs' );
?>
<?php
drupal_add_js ( 'jQuery(document).ready(function(){
jQuery("#tabs-zoom").tabs({active:0});
jQuery("#tabs").tabs();
});
' , 'inline' );
?>
<?php
drupal_add_js ( '

 function showReport(){

  	var tab_offset = jQuery("#tabs").offset().top;

  	jQuery("body,html").animate({scrollTop: tab_offset});

  }
 
   jQuery(function() {

  	var $tabs = jQuery("#tabs").tabs();
	
  	jQuery("#show-cert").click(function() {
	   
  		$tabs.tabs({active:3});

	  	showReport();

  	});

  });
' , 'inline' );
?>
<!--//Jquery ui tabs-->
<div<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['product:field_diamond_image']);
    hide($content['comments']);
    hide($content['links']);
    hide($content['title_field']);
    hide($content['field_diamond_cert_image']);
    hide($content['field_diamond_addthis']);
   // print render($content);
    ?>
    <!--left info wrapper-->
    <div class="left-info-wrapper">
        <?php print render($content['title_field']); ?>
        <!--Diamond shape diagram-->
        <!--Tabs for cloud zoom-->
        <div id="tabs-zoom">
            <ul>
                <li><a href="#tabs-zoom-1">Image</a></li>
                <li><a href="#tabs-zoom-2">Chart</a></li>
            </ul>
            <!--Tabs zoom 2-->
            <div id="tabs-zoom-1">
                <?php print render($content['product:field_diamond_image']); ?>
            </div>
            <!--//Tabs-zoom-2-->
            <div id="tabs-zoom-2">
                <div id="diam-diagram-view" class="diamond-diagram-container">
                    <!--Top view-->
                    <div class="diam-top-view"><img src="/sites/all/themes/main/images/diamond-shape/<?php print render($node->field_diamond_shape[$node->language][0]['value']); ?>_Top.jpg" alt="top view" />
                        <div id="top-view-width"><span>Width:</span><br/>
                            <?php if(!empty($node->field_diamond_measurements[$node->language][0]['value'])): ?>
                                <?php
                                $length='';
                                $width='';
                                $meas=$node->field_diamond_measurements[$node->language][0]['value'];
                                $arr=explode('x' , $meas);
                                $length=$arr[0];
                                $width=$arr[1];
                                ?>
                            <?php endif; ?>
                            <span><?php if(isset($width)) print $width; ?>mm.</span> </div>
                        <!--//top-view-width-->
                        <div id="top-view-length"> <span>Length:</span><br/>
                            <span><?php if(isset($length)) print $length; ?>mm.</span> </div>
                        <!--//Lenghth-->
                    </div>
                    <!--//Top view-->
                    <!--Side view-->
                    <div class="diam-side-view"><img src="/sites/all/themes/main/images/diamond-shape/<?php print render($node->field_diamond_shape[$node->language][0]['value']); ?>_Front.jpg" alt="side view" />
                        <div id="side-view-table"><span>Table %:</span><span>
<?php if($foo = $node->field_diamond_table[$node->language][0]['value']) print $foo; else print 'Unspec.'; ?></span>
                        </div>
                        <!--//table-->
                        <div id="side-view-culet"><span>Culet:</span><span>
<?php if($foo = $node->field_diamond_culet[$node->language][0]['value']) print $foo; else print 'Unspec.'; ?></span>
                        </div>
                        <!--//culet-->
                        <div id="side-view-depth"><span>Depth %:</span><br />
                            <span><?php if($foo = $node->field_diamond_depth[$node->language][0]['value']) print $foo; else print 'Unspec.'; ?></span>
                        </div>
                        <!--//depth-->
                        <div id="side-view-girdle"><span>Girdle:</span><br/>
<span><?php if($foo = $node->field_diamond_girdle[$node->language][0]['value']) print $foo; else print 'Unspec.'; ?>
</span>
                        </div>
                        <!--//girdle-->
                    </div>
                    <!--//side view-->
                </div>
                <!--//Diamond shape diagram-->
            </div>
            <!--//zoom-tab-1-->

            <!--Product thumb-->
        </div>
        <!--//Zoom tabs-->
    </div>
    <!--//left info wrapper-->
    <!--Right info wrapper-->
    <div class="right-info-wrapper">
        <!--Diamond info-->
        <div class="diamond-info">
            <h2><?php print render($node->field_diamond_weight[$node->language][0]['value']); ?> Carat <?php print render($node->field_diamond_shape[$node->language][0]['value']); ?> Cut Diamond</h2>
            <p>
                <?php if($tid==3): ?>
                    <?php print render ($node->field_diamond_color[$node->language][0]['value']); ?> color, <?php print render($node->field_diamond_clarity[$node->language][0]['value']); ?> clarity diamond comes with a certificate from <?php print render($node->field_diamond_lab[$node->language][0]['value']); ?>
                <?php elseif($tid==4): ?>
                    <?php print render($node->field_diamond_fancy_color[$node->language][0]['value']); ?>  <?php print render($node->field_diamond_fancy_intensity[$node->language][0]['value']); ?> color, <?php print render($node->field_diamond_clarity[$node->language][0]['value']); ?> clarity diamond comes with a certificate from <?php print render($node->field_diamond_lab[$node->language][0]['value']); ?>
                <?php endif; ?>
            </p>
		<p>
		<span>Stock ID: </span><span><?php print render($content['product:commerce_price']['#object']->sku); ?></span>
		</p>
        <p> <span class="cert-link-label"><?php print t('View certificate') . ':&nbsp;'; ?></span><span id="show-cert"><?php print render($node->field_diamond_lab[$node->language][0]['value']); ?>&nbsp;<?php print t('Report'); ?></span></p>
        </div>
        <!--Video link-->
       <!--//Diamond info-->
        <div class="mainb">
            <!--Add-to-cart-->
            <div class="add-to-cart">
              <?php print render($content); ?>
			   <!--Add to ring-->
			   <div class="add-to-ring">
				<?php print flag_create_link('add_jewely_diamonds', $node->nid); ?>
				</div>
				<!--//Add to ring-->
            </div>
            <!--//Add to cart-->
        </div>
        <div class="print-back-links">
            <!--Back to search-->
            <div class="back-to-search">
                <?php if($tid==3): ?>
                    <a href="/diamond-search">Back to search</a>
                <?php else: ?>
                    <a href="/fancy-diamond-search">Back to search</a>
                <?php endif; ?>
            </div>
            <!--//Back to Search-->
            <!--Email page-->
            <div class="print-mail">
                <?php print print_mail_insert_link(); ?>
            </div>
            <!--//Email page-->
            <!--Print page-->
            <div class="print-page">
                <?php print print_insert_link(); ?>
            </div>
            <!--//Print page-->
            <!--Enquire-->
            <div class="enquire">
                <a href="/enquire?item=<?php print 'Item: ' . $node->title; ?>">Enquire</a>
            </div>
            <!--//Enquire-->
            <!--AddThis-->
            <div class="addthis">
                <?php print render($content['field_diamond_addthis']); ?>
            </div>
            <!--//AddThis-->

        </div>
        <!--//Print-back-links-->
    </div>
    <!--//Right info wrapper-->
</div>
</div>
<!--//Content-->

<!--Tabs-->
<div id="tabs">
<ul>
    <li><a href="#tabs-1">Diamond Details</a></li>
    <li><a href="#tabs-2">You May Also Like</a></li>
    <li><a href="#tabs-3">Recently Viewed</a></li>
    <li><a href="#tabs-4">Certificate</a></li>
</ul>
<div id="tabs-1">
    <!--Diamond details fields-->
    <!--Column one-->
    <div class="diam-column-one">
        <!--Stock ID-->
        <div class="diam-details-row">
            <span class="label">Stock ID:</span>
            <span class="value"><?php print render($content['product:commerce_price']['#object']->sku); ?></span>
        </div>
        <!--//Stock ID-->
        <div class="diam-details-row">
            <span class="label">Price per carat:</span>
            <span class="value">$<?php print number_format(render($node->field_diamond_ppc[$node->language][0]['value'])); ?></span>
        </div>
        <!--//PPC-->
        <!--Rappaport-->
        <?php if($tid==3 && !empty($node->field_diamond_rapoff[$node->language][0]['value'])): ?>
            <div class="diam-details-row">
                <span class="label">%-Rap.:</span>
                <span class="value"><?php print number_format(render($node->field_diamond_rapoff[$node->language][0]['value'])); ?>&nbsp;%</span>
            </div>
        <?php endif; ?>
        <!--//Rappaport-->
        <!--Shape-->
        <div class="diam-details-row">
            <span class="label">Shape:</span>
            <span class="value"><?php print render($node->field_diamond_shape[$node->language][0]['value']); ?></span>
        </div>
        <!--//Shape-->
        <!--Carat weight-->
        <div class="diam-details-row">
            <span class="label">Carat weight:</span>
            <span class="value"><?php print render($node->field_diamond_weight[$node->language][0]['value']); ?>&nbsp;Ct.</span>
        </div>
        <!--//Carat weight-->
        <!--Colour-->
        <?php if($tid==3): ?>
            <div class="diam-details-row">
                <span class="label">Color:</span>
                <span class="value"><?php print render($node->field_diamond_color[$node->language][0]['value']); ?></span>
            </div>
        <?php elseif($tid==4): ?>
            <div class="diam-details-row">
                <span class="label">Fancy Color:</span>
                <span class="value"><?php print render($node->field_diamond_fancy_color[$node->language][0]['value']); ?></span>
            </div>
            <!--//Fancy color-->
            <div class="diam-details-row">
                <span class="label">Fancy Intensity:</span>
                <?php if(isset($node->field_diamond_fancy_intensity[$node->language][0]['value'])): ?>
                    <span class="value"><?php print render($node->field_diamond_fancy_intensity[$node->language][0]['value']); ?></span>
                <?php else: ?>
                    <span class="value">Unspecified</span>
                <?php endif; ?>
            </div>
            <!--//Fancy intensity-->
            <div class="diam-details-row">
                <span class="label">Overtone:</span>
                <?php if(isset($node->field_diamond_overtone[$node->language][0]['value'])): ?>
                    <span class="value"><?php print render($node->field_diamond_overtone[$node->language][0]['value']); ?></span>
                <?php else: ?>
                    <span class="value">Unspecified</span>
                <?php endif; ?>
            </div>
            <!--//Overtone-->
        <?php endif; ?>
        <!--//Color-->
        <!--Clarity-->
        <div class="diam-details-row">
            <span class="label">Clarity:</span>
            <?php if(isset($node->field_diamond_clarity[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_clarity[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Clarity-->
        <!--Lab-->
        <div class="diam-details-row">
            <span class="label">Lab:</span>
            <?php if(isset($node->field_diamond_lab[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_lab[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Lab-->
	
    </div>
    <!--//Column one-->
    <!--Column two-->
    <div class="diam-column-two">
	  <!--Cut-->
      <div class="diam-details-row">
      <span class="label">Cut:</span>
      <?php if(isset($node->field_diamond_cut[$node->language][0]['value'])): ?>
      <span class="value"><?php print render($node->field_diamond_cut[$node->language][0]['value']); ?></span>
      <?php else: ?>
      <span class="value">Unspecified</span>
      <?php endif; ?>
      </div>
      <!--//Cut-->	
        <!--Measurements-->
        <div class="diam-details-row">
            <span class="label">Measurements:</span>
            <?php if(isset($node->field_diamond_measurements[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_measurements[$node->language][0]['value']); ?> mm.</span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Measurements-->
        <!--Table-->
        <div class="diam-details-row">
            <span class="label">Table %:</span>
            <?php if(isset($node->field_diamond_table[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_table[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Table-->
        <!--Depth-->
        <div class="diam-details-row">
            <span class="label">Depth %:</span>
            <?php if(isset($node->field_diamond_depth[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_depth[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Depth-->
        <!--Girdle-->
        <div class="diam-details-row">
            <span class="label">Girdle:</span>
            <?php if(isset($node->field_diamond_girdle[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_girdle[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Girdle-->
        <!--Culet size-->
        <div class="diam-details-row">
            <span class="label">Culet size:</span>
            <?php if(isset($node->field_diamond_culet[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_culet[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Culet size-->
       <!--Polish-->
        <div class="diam-details-row">
            <span class="label">Polish:</span>
            <?php if(isset($node->field_diamond_polish[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_polish[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Polish-->
        <!--Symmetry-->
        <div class="diam-details-row">
            <span class="label">Symmetry:</span>
            <?php if(isset($node->field_diamond_symmetry[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_symmetry[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Symmetry-->
        <!--Symmetry-->
        <div class="diam-details-row">
            <span class="label">Fluorescence:</span>
            <?php if(isset($node->field_diamond_fluor[$node->language][0]['value'])): ?>
                <span class="value"><?php print render($node->field_diamond_fluor[$node->language][0]['value']); ?></span>
            <?php else: ?>
                <span class="value">Unspecified</span>
            <?php endif; ?>
        </div>
        <!--//Symmetry-->
    </div>
    <!--//Column three-->
</div>
<!--//Tabs-1-->
<div id="tabs-2">
<?php
  print views_embed_view('related_products', 'block_1');
?>
</div>
<!--Tabs 3-->
<div id="tabs-3">
    <?php
    print views_embed_view('recently_read', 'block_1');
    ?>
</div>
<!--//Tabs 3-->
<div id="tabs-4">
    <?php if($node->field_diamond_lab[$node->language][0]['value'] == 'GIA'): ?>
        <a class="gia-link" href="http://www.gia.edu/cs/Satellite?pagename=GST%2FDispatcher&childpagename=GIA%2FPage%2FReportCheck&c=Page&cid=1355954554547&reportno=<?php print $node->field_diamond_certnum[$node->language][0]['value']; ?>" target="_blank">GIA Online Verification</a>
    <?php elseif($node->field_diamond_lab == 'AGS'): ?>
        <a class="ags-link" href="http://agslab.com/reportTypes/pldqr.php?StoneID=<?php print $node->field_diamond_certnum[$node->language][0]['value']; ?>&Weight=<?php print $node->field_diamond_weight[$node->language][0]['value']; ?>&D=1" target="_blank">AGS Online Veryfication</a>
    <?php endif; ?>
    <!--Print button-->
    <?php if(isset($node->field_diamond_cert_image['und'][0]['uri'])): ?>
        <div class="print-cert">
            <input type="button" class="printCert" value="Print"  />
            <?php print render($content['field_diamond_cert_image']); ?>
        </div>
    <?php elseif(isset($node->field_diamond_cert_pdf['und'][0]['uri'])): ?>
        <?php $file_path=file_create_url($node->field_diamond_cert_pdf['und'][0]['uri']); ?>
        <div class="pdf-cert">
            <iframe src="<?php print $file_path; ?>" width="900" height="900"></iframe>
        </div>
    <?php endif; ?>
    <!--//Print button-->
</div>
<!--//Tabs 4-->
</div>
<!--//Tabs-->
<div class="clearfix">
    <?php if (!empty($content['links'])): ?>
        <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
</div>
</article>