<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  <?php print render($title_prefix); ?>
  <?php if (!$page && $title): ?>
  <header>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  </header>
  <?php endif; ?>

<?php print render($title_suffix); ?>
<?php if ($display_submitted): ?>
<footer class="submitted">
    <?php print $submitted; ?>
</footer>
  <?php endif; ?> 
<!--Add jquery ui tabs library-->   
<?php
drupal_add_library ( 'system' , 'ui.tabs' );
?>
<?php
drupal_add_js ( 'jQuery(document).ready(function(){
jQuery("#tabs").tabs();
});
' , 'inline' );
?>
<!--//Jquery ui tabs-->
  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  hide($content['field_diamond_cert_image']);
	  hide($content['title_field']);
	  hide($content['product:field_parcel_image']);
     // print render($content);
    ?>
<!--Left info wrapper-->
<div class="left-info-wrapper">
<?php print render($content['title_field']); ?>
<?php print render($content['product:field_parcel_image']); ?>
</div>
<!--//Left info wrapper-->
<!--Right info wrapper-->
<div class="right-info-wrapper">
<!--Diamond info-->
<div class="diamond-info">
<p>
<?php 	
foreach ($node->field_parcel_shape['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
 Shape 
<?php 	
foreach ($node->field_parcel_color['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
 Color 
<?php print $node->field_parcel_total_weight['und'][0]['value'] . ' ct.'; ?>
 parcel contains <?php print $node->field_parcel_quantity['und'][0]['value'];?> diamonds.
</p>
<p>
<span>Stock ID:&nbsp;</span><span><?php print render($content['product:commerce_price']['#object']->sku); ?></span>
</p>	
</div>
<!--//Diamond info-->
<div class="mainb">
    <!--Add-to-cart-->
    <div class="add-to-cart">
        <?php print render($content); ?>
    </div>
    <!--//Add to cart-->
</div>
<div class="print-back-links">
    <!--Back to search-->
    <div class="back-to-search">
        <a href="/parcels">Back to search</a>
    </div>
    <!--//Back to Search-->

    <!--Email page-->
    <div class="print-mail">
        <?php print print_mail_insert_link(); ?>
    </div>
    <!--//Email page-->

    <!--Print page-->
<div class="print-page">
<?php print print_insert_link(); ?>
</div>
<!--//Print page-->
<!--Enquire-->
<div class="enquire">
<a href="/enquire?item=<?php print 'Item: ' . $node->title; ?>">Enquire</a>
</div>
<!--//Enquire-->
</div>
<!--//Print-back-links-->
</div>
<!--//Right info wrapper-->	
</div>
<!--//Content-->

<!--Tabs-->  
<div id="tabs">
<ul>
<li><a href="#tabs-1">Parcel Details</a></li>
<li><a href="#tabs-2">Other Parcels</a></li>
</ul>
<div id="tabs-1">
<!--Shape-->
<div class="diam-details-row">
<span class="label">Shape:</span>
<?php if(isset($node->field_parcel_shape['und'][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_shape['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
</div>
<!--//Shape-->
<!--Color-->
<div class="diam-details-row">
<span class="label">Color:</span>
<?php if(isset($node->field_parcel_color['und'][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_color['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
</div>
<!--//Color-->
<!--Clarity-->
<div class="diam-details-row">
<span class="label">Clarity:</span>
<?php if(isset($node->field_parcel_clarity['und'][0]['value'])): ?>
<?php 	
foreach ($node->field_parcel_clarity['und'] as $val){
 print $val['value'];
 print ' ';
}
?>
<?php endif; ?>
</div>
<!--//Clarity-->
<!--Average weight-->
<div class="diam-details-row">
<span class="label">Average Weight:</span>
<?php if(isset($node->field_parcel_average_weight['und'][0]['value'])): ?>
<?php 	
print $node->field_parcel_average_weight['und'][0]['value'] . ' Ct.';
?>
<?php endif; ?>
</div>
<!--//Average weight-->
<!--Total weight-->
<div class="diam-details-row">
<span class="label">Total Weight:</span>
<?php if(isset($node->field_parcel_total_weight['und'][0]['value'])): ?>
<?php 	
print $node->field_parcel_total_weight['und'][0]['value'] . ' Ct.';
?>
<?php endif; ?>
</div>
<!--//Total weight-->
<!--Quantity-->
<div class="diam-details-row">
<span class="label">Quantity:</span>
<?php if(isset($node->field_parcel_quantity['und'][0]['value'])): ?>
<?php 	
print $node->field_parcel_quantity['und'][0]['value'] . ' Ct.';
?>
<?php endif; ?>
</div>
<!--//Quantity-->
</div>
<!--//Tabs-1-->
<div id="tabs-2">
<?php
print views_embed_view('related_products', 'block_2');
?>
</div>
</div>
<!--//Tabs-->
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>
    <?php print render($content['comments']); ?>
  </div>
</article>
