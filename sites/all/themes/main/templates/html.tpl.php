<?php print $doctype; ?>
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf->version . $rdf->namespaces; ?>>
<head<?php print $rdf->profile; ?>>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
   <!-- <link type="text/stylesheet" rel="stylesheet" href="/sites/all/themes/main/css/font-awesome.css">-->
   <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> 
    <!--  <link type="text/stylesheet" rel="stylesheet" href="/sites/all/themes/main/css/easystock.css">-->
    <link type="text/stylesheet" rel="stylesheet" href="/sites/all/themes/main/css/responsive.css">
    
    <link rel="shortcut icon" href="">
</head>
<body<?php print $attributes;?>>
<div id="mainwrapper">
    <?php
    global $user;
    if ($user->uid) { ?>
    <div style="height: 23px;"></div>
    <?php } ?>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
</div>
<div id="homefooter">
    <div class="homefooter-wrapper">

    </div>
</div>


<div id="footer">

    <div class="footer-wrapper"> <div id="block-block-4" class="clear-block block block-block">
            <div class="content"><div class="footer-left">
                    <div class="top">
                        <p class="ycs-logo">- <a href="http://ycs.co.il/" target="_black">Yahalom Creative Solutions, Ltd. Leveraging Technology.</a></p>
                        <h4>
                            About YCS</h4>
                        <p>YCS consists of branding, marketing, and creative experts. We provide corporate branding, website design, and E-commerce solutions. YCS builds brands within both the B2B and B2C business sectors, aiming to make them well-known in their fields of activity.</p>
                    </div>
                    <div class="bottom">
                        <h5>
                            Contact Us</h5>
                        <p>8 Dov Friedman St., Ramat Gan, ISRAEL<br>
                            P: 972-3-7522322 | E: <a href="mailto:info@easystock.co.il">info@easystock.co.il</a></p>
                    </div>
                </div>
                <div class="footer-right">
                    <div class="top">
                        <h4>
                            Need Help?</h4>
                        <p>Our commerce experts are standing by.<br>
                            <span class="phone">Call: 972-3-7522322</span></p>
                    </div>
                    <div class="bottom">
                        <a class="fb" href="https://www.facebook.com/easystockdiam" target="_blank">fb</a> <a class="tw" href="https://twitter.com/#!/mosheelkayam" target="_blank">tw</a> <a class="in" href="http://www.linkedin.com/profile/edit?trk=hb_tab_pro_top" target="_blank">in</a></div>
                </div>
                <div class="clear-both">
                    &nbsp;</div>
            </div>
        </div>

        <div class="clear-both"> </div>

    </div>

</div>
<div id="closure-blocks" class="region region-closure">

    <div class="closure-wrapper"><div id="block-views-footer_links-block_1" class="clear-block block block-views">
            <div class="content"><div class="view view-footer-links view-id-footer_links view-display-id-block_1 view-dom-id-3">



                    <div class="view-content">
                        <div class="views-row views-row-1 views-row-odd views-row-first">
                            <div class="views-field-title">
                                <span class="field-content">Overview</span>
                            </div>

                            <div class="views-field-field-footer-link-url">
                                <div class="field-content"><div class="field-item field-item-0"><a href="/why-easystock">E-commerce software</a></div><div class="field-item field-item-1"><a href="/about-easystock">About Us</a></div><div class="field-item field-item-2"><a href="/portfolio">Portfolio</a></div><div class="field-item field-item-3"><a href="/quick-tour">Quick Tour</a></div></div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="views-row views-row-2 views-row-even">
                            <div class="views-field-title">
                                <span class="field-content">E-commerce solutions</span>
                            </div>

                            <div class="views-field-field-footer-link-url">
                                <div class="field-content"><div class="field-item field-item-0"><a href="/easystock-pro-diamond-b2b-and-b2c-e-commerce-software">EasyStock Pro</a></div><div class="field-item field-item-1"><a href="/easystock-jewelry-e-commerce-b2b-and-b2c-software">EasyStock Jewelry</a></div><div class="field-item field-item-2"><a href="/easystock-community-unique-website-organization">EasyStock Community</a></div><div class="field-item field-item-3"><a href="/easystock-lite-diamond-e-commerce-software">EasyStock Lite</a></div><div class="field-item field-item-4"><a href="/stay-connected-mobile-optimized">Mobile Optimized</a></div></div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <div class="views-row views-row-3 views-row-odd views-row-last">
                            <div class="views-field-title">
                                <span class="field-content">Services</span>
                            </div>

                            <div class="views-field-field-footer-link-url">
                                <div class="field-content"><div class="field-item field-item-0"><a href="/website-development">E-commerce web development</a></div><div class="field-item field-item-1"><a href="/website-design">E-commerce web design</a></div><div class="field-item field-item-2"><a href="/e-commerce-solutions">E-commerce solutions</a></div><div class="field-item field-item-3"><a href="/e-commerce-development">E-commerce development</a></div></div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>


                </div> </div>
        </div>
        <div id="block-menu-secondary-links" class="clear-block block block-menu">
            <div class="content"><ul class="menu"><li class="leaf first last"><a href="/sitemap" title="Display a site map with RSS feeds.">Site map</a></li>
                </ul></div>
        </div>
        <div id="block-block-5" class="clear-block block block-block">
            <div class="content"><p>© Copyright 2003-2014 EasyStock. All rights reserved.</p>
            </div>
        </div>

        <div class="clear-both"> </div>

    </div>

</div>
</body>
</html>
