<?php

/**
 * @file
 * Default print module template
 *
 * @ingroup print
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $print['language']; ?>" xml:lang="<?php print $print['language']; ?>">
  <head>
    <?php print $print['head']; ?>
    <?php print $print['base_href']; ?>
    <title><?php print $print['title']; ?></title>
    <?php print $print['scripts']; ?>
    <?php print $print['sendtoprinter']; ?>
    <?php print $print['robots_meta']; ?>
    <?php print $print['css']; ?>
  </head>
  <body>
  <!--Get Diamond node tid-->   
<?php 
$tid = $node->field_diamond_category[$node->language][0]['tid']; 
?>
    <?php 
	$prod_id = $node->field_product['und'][0]['product_id'];
	$product = commerce_product_load($prod_id);
	//print'<pre>'; 
	//print_r($product); 
	//print'</pre>';
    $amount=$product->commerce_price['und'][0]['amount'];
    $currency_code=$product->commerce_price['und'][0]['currency_code'];
	$price_full=commerce_currency_format($amount, $currency_code);
	$price_split=explode('.' , $price_full); 
	$price=$price_split[0];
	?>
	<?php if (!empty($print['message'])) {
      print '<div class="print-message">'. $print['message'] .'</div><p />';
    } ?>
    <div class="print-logo"><?php print $print['logo']; ?></div>
    <p />
    <hr class="print-hr" />
	<h2><?php print $node->field_diamond_weight[$node->language][0]['value']; ?> Carat <?php print $node->field_diamond_shape[$node->language][0]['value']; ?> Cut Diamond</h2>
	<!--Content-->
    <div class="print-content">
	<?php /*?><?php print $print['content']; ?><?php */?>
	<div>
	<p>
	<?php if($tid==3): ?>
    <?php print $node->field_diamond_color[$node->language][0]['value']; ?> color, <?php print $node->field_diamond_clarity[$node->language][0]['value']; ?> comes with a certificate from <?php print    $node->field_diamond_lab[$node->language][0]['value']; ?>
	<?php elseif($tid==4): ?>
	<?php print $node->field_diamond_fancy_color[$node->language][0]['value']; ?>  <?php print $node->field_diamond_fancy_intensity[$node->language][0]['value']; ?> color, <?php print $node->field_diamond_clarity[$node->language][0]['value']; ?> clarity diamond comes with a certificate from <?php print $node->field_diamond_lab[$node->language][0]['value']; ?>
	<?php endif; ?>
    </p>
	</div>
	<div>
	<span class="print-label">Stock ID: </span><span class="print-value"><?php print render($product->sku); ?></span>
	</div>
	<!--//Stock ID-->
	<div>
	<span class="print-label">Shape: </span><span class="print-value"><?php print render($node->field_diamond_shape['und'][0]['value']); ?></span>
	</div>
	<!--//Shape-->
	<div>
	<span class="print-label">Carat weight: </span><span class="print-value"><?php print render($node->field_diamond_weight['und'][0]['value']); ?> Ct.</span>
	</div>
	<!--//Weight-->
	<?php if($tid==3): ?>
	<div>
	<span class="print-label">Color: </span><span class="print-value"><?php print render($node->field_diamond_color['und'][0]['value']); ?></span>
	</div>
	<?php elseif($tid==4): ?>
	<div>
	<span class="print-label">Fancy Color: </span><span class="print-value"><?php print render($node->field_diamond_fancy_color['und'][0]['value']); ?></span>
	</div
	><div>
	<span class="print-label">Fancy Intensity: </span><span class="print-value"><?php print render($node->field_diamond_fancy_intensity['und'][0]['value']); ?></span>
	</div
	><?php endif; ?>
	<!--//Color-->
	<div>
	<span class="print-label">Clarity: </span><span class="print-value"><?php print render($node->field_diamond_clarity['und'][0]['value']); ?></span>
	</div>
	<!--//Clarity-->
	<div>
	<span class="print-label">Measurements: </span><span class="print-value"><?php print render($node->field_diamond_measurements['und'][0]['value']); ?> mm.</span>
	</div>
	<!--//Measurements-->
	<?php if($node->field_diamond_table['und'][0]['value']): ?>
	<div>
	<span class="print-label">Table %: </span><span class="print-value"><?php print render($node->field_diamond_table['und'][0]['value']); ?></span>
	</div>
	<?php endif;?>
	<!--//Table-->
	<?php if($node->field_diamond_depth['und'][0]['value']): ?>
	<div>
	<span class="print-label">Depth %: </span><span class="print-value"><?php print render($node->field_diamond_depth['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Depth-->
	<?php if($node->field_diamond_girdle['und'][0]['value']): ?>
	<div>
	<span class="print-label">Girdle: </span><span class="print-value"><?php print render($node->field_diamond_girdle['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Girdle-->
	<?php if($node->field_diamond_culet['und'][0]['value']): ?>
	<div>
	<span class="print-label">Culet size: </span><span class="print-value"><?php print render($node->field_diamond_culet['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Culet size-->
	<?php if($node->field_diamond_cut['und'][0]['value']): ?>
	<div>
	<span class="print-label">Cut: </span><span class="print-value"><?php print render($node->field_diamond_cut['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Cut-->
	<?php if($node->field_diamond_polish['und'][0]['value']): ?>
	<div>
	<span class="print-label">Polish: </span><span class="print-value"><?php print render($node->field_diamond_polish['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Polish-->
	<?php if($node->field_diamond_symmetry['und'][0]['value']): ?>
	<div>
	<span class="print-label">Symmetry: </span><span class="print-value"><?php print render($node->field_diamond_symmetry['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Symmetry-->
	<?php if($node->field_diamond_fluor['und'][0]['value']): ?>
	<div>
	<span class="print-label">Fluor. intensity: </span><span class="print-value"><?php print render($node->field_diamond_fluor['und'][0]['value']); ?></span>
	</div>
	<?php endif; ?>
	<!--//Fluor.-->
	<div>
	<span class="print-label">Price per carat: </span><span class="print-value">$<?php print number_format(render($node->field_diamond_ppc['und'][0]['value'])); ?></span>
	</div>
	<!--//PPC-->
	<div>
	<span class="print-label">Total Price: </span><span class="print-value"><?php print $price; ?></span>
	</div>
	<!--//Total price-->
	<!--Cert.-->
	<div>
	<br />
	<div>
	<span class="print-label">Certificate: </span><span class="print-value"><?php print render($node->field_diamond_certnum['und'][0]['value']); ?></span>
	</div>
	<!--//Certificate-->
	<br />
	<img src="<?php print file_create_url($node->field_diamond_cert_image['und'][0]['uri']); ?>"  width="800" />
	</div>
	<!--//Cert-->
	</div>
	<!--//Content-->
    <div class="print-footer"><?php print $print['footer_message']; ?></div>
    <hr class="print-hr" />
    <?php print $print['footer_scripts']; ?>
  </body>
</html>
