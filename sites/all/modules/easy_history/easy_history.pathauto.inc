<?php

/**
 * @file
 * Contains hook implementations for integration with the Pathauto module.
 */

/**
 * Implements hook_path_alias_types().
 */
function easy_history_path_alias_types() {
  $objects['views-save/'] = t('Saved views');
  return $objects;
}

/**
 * Implements hook_pathauto().
 */
function easy_history_pathauto($op) {
  switch ($op) {
    case 'settings':
      $settings = array();
      $settings['module'] = 'easy_history';
      $settings['token_type'] = 'easy_history';
      $settings['groupheader'] = t('Views Save paths');
      $settings['patterndescr'] = t('Default path pattern (applies to all saved view types with blank patterns below)');
      $settings['patterndefault'] = 'saved-views/[easy_history:title]';
      $settings['batch_update_callback'] = 'easy_history_pathauto_bulk_update_batch_process';
      $settings['batch_file'] = drupal_get_path('module', 'easy_history') . '/easy_history.pathauto.inc';

      return (object) $settings;

    default:
      return NULL;
  }
}

/**
 * Batch processing callback to generate aliases for saved views.
 */
function easy_history_pathauto_bulk_update_batch_process(&$context) {
  if (!isset($context['sandbox']['current'])) {
    $context['sandbox']['count'] = 0;
    $context['sandbox']['current'] = 0;
  }

  $query = db_select('easy_history', 'v');
  $query->leftJoin('url_alias', 'ua', "CONCAT('easy_history/', v.id) = ua.source");
  $query->addField('v', 'id');
  $query->isNull('ua.source');
  $query->condition('v.id', $context['sandbox']['current'], '>');
  $query->orderBy('v.id');
  $query->addTag('pathauto_bulk_update');
  $query->addMetaData('entity', 'easy_history');

  // Get the total amount of items to process.
  if (!isset($context['sandbox']['total'])) {
    $context['sandbox']['total'] = $query->countQuery()->execute()->fetchField();

    // If there are no saved views to update, the stop immediately.
    if (!$context['sandbox']['total']) {
      $context['finished'] = 1;
      return;
    }
  }

  $query->range(0, 25);
  $nids = $query->execute()->fetchCol();

  pathauto_easy_history_update_alias_multiple($nids, 'bulkupdate');
  $context['sandbox']['count'] += count($nids);
  $context['sandbox']['current'] = max($nids);
  $context['message'] = t('Updated alias for easy_history @nid.', array('@nid' => end($nids)));

  if ($context['sandbox']['count'] != $context['sandbox']['total']) {
    $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
  }
}

/**
 * Update the URL aliases for multiple saved views.
 *
 * @param array $nids
 *   An array of easy_history IDs.
 * @param string $op
 *   Operation being performed on the saved views ('insert', 'update' or
 *   'bulkupdate').
 * @param array $options
 *   An optional array of additional options.
 */
function pathauto_easy_history_update_alias_multiple(array $nids, $op, array $options = array()) {
  $options += array('message' => FALSE);

  $saves = easy_history_load_multiple($nids);
  foreach ($saves as $save) {
    easy_history_update_alias($save, $op, $options);
  }

  if (!empty($options['message'])) {
    drupal_set_message(format_plural(count($nids), 'Updated URL alias for 1 easy_history.', 'Updated URL aliases for @count saved views.'));
  }
}
