(function ($) {
  Drupal.behaviors.saved_update_view = {
    attach: function(context, settings) {
      $('td.views-field-view-path').each(function(){
        if ($(this).parents('#views-live-preview').length>0) return ;
        var m = /^\s*(.+)\?(.+)\s*$/.exec(this.innerHTML.replace(/&amp;/g, '&'));
        console.info(this.innerHTML);
        var html = '<b>' + m[1] + '</b><dl>'
        var options = m[2].split('&');
        options.forEach(function(o) {
          html += '<dt>' + o.split('=')[0] + '</dt><dd>' + decodeURIComponent(o.split('=')[1]) + '</dd>';
        });
        html += '</dl>';
        this.innerHTML = html;
      });
    }
  }
})(jQuery);
