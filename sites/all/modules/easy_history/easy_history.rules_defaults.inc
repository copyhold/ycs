<?php

/**
 * Implements hook_default_rules_configuration().
 */
function easy_history_default_rules_configuration() {
  // Set order status : Canceled
  $rules["rules_send_diamond_list_from_saved_view"] = entity_import('rules_config', '{ "rules_send_diamond_list_from_saved_view" : {
    "LABEL" : "Send diamond list from saved view",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "Custom" ],
    "REQUIRES" : [ "easy_history", "simplenews_rules", "rules" ],
    "USES VARIABLES" : { "save" : { "label" : "Save", "type" : "easy_history" } },
    "ACTION SET" : [
      { "easy_history_rules_action_unsubscribe_all" : { "tid" : "5" } },
      { "simplenews_rules_action_subscribe" : { "mail" : [ "save:user:mail" ], "tid" : "5", "confirmation" : "2" } },
      { "redirect" : { "url" : "admin\/content\/newsletter-creation\/saved-view\/[save:id]" } }
    ]
  }
}');
  return $rules;
}

