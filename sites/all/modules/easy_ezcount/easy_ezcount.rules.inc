<?php
function easy_ezcount_rules_action_info() {
  return [
    "easy_ezcount_action_send_memoin" => [
      'label' => 'Sends the memo in doc',
      'group' => 'EasyStock',
      'parameter' => [
        'memoin' => [
          'label' => 'Document',
          'type' => 'node'
        ]
      ],
    ],
    "easy_ezcount_action_send_memoout" => [
      'label' => 'Sends the memo out doc',
      'group' => 'EasyStock',
      'parameter' => [
        'memoout' => [
          'label' => 'Document',
          'type' => 'node'
        ]
      ],
    ],
    "easy_ezcount_action_send_invoice" => [
      'label' => 'Sends the invoice doc',
      'group' => 'EasyStock',
      'parameter' => [
        'order' => [
          'label' => 'Order',
          'type' => 'commerce_order'
        ]
      ],
    ],
    "easy_ezcount_action_send_order" => [
      'label' => 'Send invoice-receipt to EZC',
      'group' => 'EasyStock',
      'parameter' => [
        'order' => [
          'label' => 'Order',
          'type' => 'commerce_order'
        ]
      ],
    ]
  ];
}
function easy_ezcount_action_send_memoout($memoout) {
  $ezc = new MemoOut($memoout);
  $ezc->prepare();
  $ezc->save();
}
function easy_ezcount_action_send_memoin($memoin) {
  $ezc = new MemoIn($memoin);
  $ezc->prepare();
  $ezc->save();
}
function easy_ezcount_action_send_invoice($order) {
  if (variable_get('ezc_invoice_or_order', 1)) {
    $ezc = new EzcInvoice($order);
  } else {
    $ezc = new EzcOrder($order);
  }
  $ezc->prepare();
  $ezc->save();
}
function easy_ezcount_action_send_order($order) {
  $ezc = new EzcReceipt($order);
  $ezc->prepare();
  $ezc->save();
}
