<?php
# @INFO
# https://goo.gl/ZfXGsc
# 100, הזמנה (Order), 
# 200, תעודת משלוח (Delivery), 
# 210, תעודת החזרה (Return), 
# 300, חשבונית עסקה (Proforma Invoice), 
# 305, חשבונית מס(Tax invoice), 
# 320, חשבונית מס קבלה(Invoice Receipt), 
# 330, חשבונית זיכוי(Credit invoice), 
# 400, קבלה(Receipt), 
# 405, קבלה על תרומה(Receipt for donation)
# 500, הזמנת רכש(Purchase order), 
# 9999, הצעת מחיר(Bid)
abstract class Ezc {
  const API_KEY = 'fc6b1e111545eb95a1b4716b4c1cbb837c5ccb12743201fabf4d49c1afd8d5f0';

  abstract protected function prepare();
  protected  $order, $doctype, $node, $doc, $rel, $params;

  public function __construct($order) {
    $this->order = entity_metadata_wrapper(static::BUNDLE, $order);
  }
  public function save() {
    try {
      $this->create_doc();
      $this->create_node();
      $this->create_relation();
    } catch (Exception $e) {
      watchdog('ezcount', $e->getMessage());
    }
  }

  private function create_doc() {
//  $this->doc = [
//    'pdf_link'=>'link',
//    'pdf_link_copy'=>'link copy',
//    'doc_uuid'=>'uuid',
//    'doc_number'=>'1111'
//  ];
//  return;
    drupal_set_message(json_encode($this->params));
    $ch = curl_init(EZC_URL . '/api/createDoc');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->params));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $res = curl_exec($ch);
    watchdog('ezcount', $res, WATCHDOG_DEBUG);

    $res = json_decode($res,1);
    if (array_key_exists('errNum',$res)) {
      throw new Exception('Push to EasyCount failed');
    } else { // all cool - update the order
      $this->doc = $res;
    }
  }

  private function create_node() {
    $doc = entity_create('node', ['type' => 'ezcount']);
    $wr = entity_metadata_wrapper('node', $doc);
    $wr->title = $this->doc['doc_uuid'];
    $wr->ezcdoctype = static::DOCTYPE;
    $wr->field_ezc_pdf_link->set($this->doc['pdf_link']);
    $wr->field_ezc_pdf_link_copy->set($this->doc['pdf_link_copy']);
    $wr->field_ezc_doc_uuid->set($this->doc['doc_uuid']);
    $wr->field_ezc_doc_num->set($this->doc['doc_number']);
    $wr->save();
    $this->node = $wr;
    return $wr;
  }

  private function create_relation() {
    $endpoints = [
      ['entity_type' => 'node', 'entity_id' => $this->node->getIdentifier()],
      ['entity_type' => $this->order->type(), 'entity_id' => $this->order->getIdentifier()]
    ];
    $rel = relation_create(EZCRELATION, $endpoints);
    if (!relation_save($rel)) {
      throw new Exception('Create relation failed');
    } else {
      var_dump($rel);
      $this->rel = $rel;
    }
  }
  protected function default_params($date) {
    return [
      'api_key'         => variable_get('company_ezcount_id', Ezc::API_KEY),
      'api_email'       => variable_get('ezcount_company_email', ''),
      'developer_email' => variable_get('ezcount_devemail', 'novojilov.ilya@gmail.com'),
      'dont_send_email' => 1,
      'date'            => date('d/m/Y', $date),
    ];
  }
}
// --------------------- MEMOIN ---------------------
class MemoIn extends Ezc {
  const DOCTYPE = 'memoin';
  const DOCCODE = 9997;
  const BUNDLE = 'node';

  public function prepare() {
    $wrapper = entity_metadata_wrapper('node', $this->order);
    if ($wrapper->type->value()!=static::DOCTYPE) {
      $message = 'not valid node type for '.static::DOCTYPE;
      watchdog('error', $message);
      drupal_set_message('EZCount document not created due to error', 'error');
    }
    $items = [];
    foreach ($wrapper->field_gems->getIterator() as $delta=>$product_wrapper) {
      $product = $product_wrapper->field_product[0];
      $items[] = [
        'catalog_number' => $product->sku->value(),
        'details'        => $product->title_field->value(),
        'amount'         => 1,
      //'price'          => $product->commerce_price->amount->value()
        'price'          => 0
      ];
    }
    
    $this->params = array_merge($this->default_params($wrapper->created->value()), [
      'customer_name'  => $wrapper->field_memo_agent->field_profile_company->value(),
      'customer_email' => $wrapper->field_memo_agent->mail->value(),
      'transaction_id' => $wrapper->title->value() . '/' . $wrapper->getIdentifier(),
      'type'           => static::DOCCODE,
      'item'           => $items,
      'payment'        => [
        'payment_type'=> 9,
        'other_payment_type_name' => 'not relevant',
        'date' => date('d/m/Y'),
        'payment' => 0
      ]
    ]);
    return $this->params;
    
  }
}
// --------------------- MEMOOUT ---------------------
class MemoOut extends MemoIn {
  const DOCTYPE = 'memoout';
  const DOCCODE = 9998;
}
// --------------------- INVOICE ---------------------
class EzcInvoice extends Ezc {
  const DOCTYPE = 'invoice';
  const DOCCODE  = 305;
  const BUNDLE = 'commerce_order';

  public function prepare() {
    $wrapper = $this->order;
    // @TODO check if relation exists by using entity_find
    $order_currency = $wrapper->commerce_order_total->currency_code->value();
    switch ($order_currency) {
    case 'ILS':
      $carrrate = 1;
      break;
    case 'USD':
      $carrrate = 3.5;
      break;
    default: 
      $carrrate = 3.5;
    }
    define('CURR_RATE', $carrrate);


    $total = $wrapper->commerce_order_total->value();
    foreach($total['data']['components'] as $component) {
      if ($component['included'] && preg_match('~^tax~', $component['name'])) {
        $taxcomponent = $component;
      }
    }
    $baseprice = commerce_price_component_total($total , 'base_price')['amount'];
    if ($taxcomponent) {
      $tax = commerce_price_component_total($total, 'tax|israel');
      $tax = commerce_currency_amount_to_decimal($tax['amount'],$tax['currency_code']);
      $taxrate = $taxcomponent['price']['data']['tax_rate']['rate'];
      $total = commerce_currency_amount_to_decimal($baseprice, $order_currency) + $tax;
    } else {
      $tax = 0;
      $taxrate = 0;
    }
    $custname = $wrapper->owner->field_profile_company->value();
    if (!$custname) {
      $custname = $wrapper->owner->field_profile_first_name->value() . ' ' . $wrapper->owner->field_profile_last_name->value();
    }

    $billing = $wrapper->commerce_customer_billing->commerce_customer_address;
    $this->params = array_merge($this->default_params($wrapper->created->value()), [
      'type'               => static::DOCCODE, // invoice receipt
      'transaction_id'     => $wrapper->order_number->value() . '/' . static::DOCTYPE,
      'customer_name'      => $billing->name_line->value(),
      'customer_address'   => $billing->thoroughfare->value(),
      'customer_email'     => $wrapper->mail->value(),
      'customer_phone'     => $wrapper->commerce_customer_billing->field_customer_phone->value(),
      'price_total'        => $total,
      'dont_send_email'    => 1,
      'main_currency_rate' => CURR_RATE,
      'main_currency_iso'  => $order_currency, // usd || eur || gbp
    ]);

    if ($taxrate) {
      $this->params['vat'] = $taxrate * 100;
    }

    $this->params['item'] = $this->collect_products($taxrate);
    $this->params['payment'] = $this->collect_order_payments(); 
    return $this->params;
  }
  protected function collect_products() {
    $wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $items = [];
    $order_currency = $wrapper->commerce_order_total->currency_code->value();
    foreach ($wrapper->commerce_line_items as $delta => $line_item_wrapper) {
      $price = commerce_currency_amount_to_decimal($line_item_wrapper->commerce_unit_price->amount->value(), $order_currency);
      $title = $line_item_wrapper->commerce_product->title->value();
      $items[] = [ 
        'amount' => 1,
        'catalog_number' => $line_item_wrapper->commerce_product->sku->value(),
        'details' => $title,
        'vat_type' => 'INC',
        'price' => $price
      ];
    }
    return $items;
  }
  protected function collect_order_payments() {
    $wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    $order_payments = commerce_payment_transaction_load_multiple(array(), array('order_id' =>  $wrapper->getIdentifier()));
    $payments = array_map(function($payment) {
      /**
       * 1 - cash
       * 2 - check
       * 3 - credit card
       * 4 - bank transfer
       * 9 - Other
       */
      return [
        'payment_type'=> 9, // define by switch $payment->payment_method
        'other_payment_type_name' => $payment->payment_method,
        'date' => date('d/m/Y', $payment->created),
        'payment' => $payment->amount / 100,
      //'currency' => $payment->currency_code,
      //'currency_rate' => CURR_RATE
      ];
    }, $order_payments);
    return $payments;
  }
}

// --------------------- ORDER ---------------------
class EzcOrder extends EzcInvoice {
  const DOCCODE  = 300;
}
// --------------------- RECEIPT ---------------------
class EzcReceipt extends EzcInvoice {
  const DOCTYPE = 'receipt';
  const DOCCODE  = 400;
  public function prepare() {
    parent::prepare();
    $q = relation_query('commerce_order', $this->order->getIdentifier());

    $rids = array_keys($q->execute());
    foreach($rids as $rid) {
      $relation = relation_load($rid);
      $node = entity_metadata_wrapper('node', $relation->endpoints['und'][0]['entity_id']);
      if ($node->getBundle()!='ezcount' || $node->ezcdoctype->value()!='invoice') continue;

      $uuid = $node->field_ezc_doc_uuid->value();
      $this->params['parent'] = $uuid;
    }
    return $this->params;
  }
}
