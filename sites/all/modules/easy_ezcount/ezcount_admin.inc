<?php
function ezcount_settings($form_id, &$form_state) {
  $form = [
    'ezcount_demo_mode' => [ '#type' => 'checkbox' , '#default_value' => variable_get('ezcount_demo_mode', true), '#title' => 'Demo mode' ],
    'ezcount_apikey'    => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_apikey', Ezc::API_KEY), '#title' => 'EZCount API key' ],
    'ezcount_apiemail'  => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_apiemail', ''), '#title' => 'EZCount API email' ],
    'ezcount_devemail'  => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_devemail', 'novojilov.ilya@gmail.com'), '#title' => 'EZCount developer email' ],
  ];
  return system_settings_form($form);
}

function ezcount_company_settings($form_id, &$form_state) {
  $logo=variable_get('ezcount_company_logo', '');
  $logo = file_load($logo);

  $logo_url = file_create_url($logo->uri);
  $form = [
    'company_ezcount_id'    => [ '#type' => 'textfield', '#value' => variable_get('company_ezcount_id', ''), '#title' => 'EZCount company API KEY' , '#attributes' => [ 'disabled' => true ]],
    'company_ezcount_uuid'  => [ '#type' => 'textfield', '#value' => variable_get('ezcount_uuid',       ''), '#title' => 'EZCount company UUID'    , '#attributes' => [ 'disabled' => true ]],
    'create_company'        => [ '#type' => 'submit',    '#value' => 'Create', '#submit' => ['ezcount_company_settings_sab'], '#suffix' => 'Click button to save company in EZCount', 
                                 '#disabled' => variable_get('ezc company created', 0) ],
    'ezcount_company_cm'    => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_company_cm',    '999999999'),           '#required' => true, '#title' => 'Company ID'    ],
    'ezcount_company_name'  => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_company_name',  'Demo EAsy company'),   '#required' => true, '#title' => 'Company Name'  ],
    'ezcount_company_email' => [ '#type' => 'textfield', '#default_value' => variable_get('ezcount_company_email', 'company@example.com'), '#required' => true, '#title' => 'Company email' ],
    'ezcount_company_type'  => [ '#type' => 'select', '#default_value' => variable_get('ezcount_company_type', 1),
                                 '#options' => [ 1 => 'osek murshe', 2 => 'osek patur', 3 => 'baam' , 4 => 'amuta' ]
    ],
    'ezc_invoice_or_order' => [ '#type' => 'checkbox', '#default_value' => variable_get('ezc_invoice_or_order', null), '#title' => 'Invoice/Order', '#description' => 'Check this box if your site provides payments' ],
    'ezcount_company_logo' => [ '#type' => 'managed_file', '#default_value' => variable_get('ezcount_company_logo_fid', ''), '#upload_location' => 'public://companylogo/', '#title' => 'Company logo' ],
  ];
  $form['#submit'][] = 'ezc_save_company_logo';
  return system_settings_form($form);
}

function ezc_save_company_logo($form, &$form_state) {
  if (is_numeric($form_state['values']['ezcount_company_logo'])) {
    // Load the file via file.fid.
    $file = file_load($form_state['values']['ezcount_company_logo']);
    if ($file) {
      // Change status to permanent.
      $file->status = FILE_STATUS_PERMANENT;
      // Save.
      file_save($file);
      // Save file to variable.
      variable_set('ezcount_company_logo_fid', $file->fid);
      // Record that the module (in this example, user module) is using the file. 
      file_usage_add($file, 'easy_ezcount', 'company', $file->fid);
      // Unset formstate value.
      unset($form_state['values']['ezcount_company_logo']); // make sure it is unset before system submit
    }
  }
  else {
    // Delete file
    variable_set('ezcount_company_logo_fid', '');
  }
}

function ezcount_company_settings_sab($form_id, &$form_state) {
  global $base_url;
  $logo=variable_get('ezcount_company_logo_fid', '');
  $logo = file_load($logo);
  $logo_url = file_create_url($logo->uri);

  $ch = curl_init(EZC_URL . '/api/user/create');
  $params = [
    'api_key' => variable_get('ezcount_apikey', Ezc::API_KEY),
    'api_email' => variable_get('ezcount_apiemail', ''),
    'create_segnature' => 1,
    'developer_email' => variable_get('ezcount_devemail', 'novojilov.ilya@gmail.com'),
    'company_crn' => variable_get('ezcount_company_cm', ''),
    'company_email' => variable_get('ezcount_company_email', ''),
    'company_name' => variable_get('ezcount_company_name', ''),
    'company_type' => variable_get('ezcount_company_type', 1),
    'user_key' => base64_encode(variable_get('ezcount_company_name', '')), 
  ];
  if (!preg_match('/localhost/', $logo_url)) {
    $params['logo_url'] = $logo_url;
  }
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

  $res = curl_exec($ch);
  
  try {
    $res = json_decode($res,1);
    if (!$res['success']) throw new Exception('no success');
    variable_set('ezcount_uuid', $res['u_uuid']);
    variable_set('company_ezcount_id', $res['u_api_key']);
    variable_set('ezc company created', true);
  } catch (Exception $e) {
    drupal_set_message('Company not created due to error: ' . json_encode($res), 'error');
  }

  // drupal_goto('admin/config/ezcount/company');
  // else - call ezcount for create|update company ( depending on companyid )
  // show message and fail validation
}
