<?php

/* @WORKFLOW
 *
 ** memo in to import gems to the stock ( sent to ezcount , memo in doc updated )
 *  ....
 *  order from client
 *      order status checkout
 **       send invoice|order  to ezcount - order updated with invoice reference ( set in settings )
 *      order status checkout comple ( i.e. payed )
 *        send invoice receipt to ezcount - order updated with 320 KABALA reference
 *      order status completed (i.e. sent to client )
 *        need to remove gems from stock so delivery 200  doc sent to ezcount
 *        order updated with delivery reference, gems removed from stock.
 ** memo out can be created without any reason - return to supplier.
 *  in this case MEMO OUT doc created in drupal with reference to ezcount document.
 */

function easy_ezcount_default_rules_configuration() {
  $invoice = rules_reaction_rule();
  $invoice->label =  "Submit Invoice/Order to EZCount";
  $invoice->active = true;
  $invoice
    ->event('commerce_order_update')
    ->condition('data_is', [ 'data:select' => 'commerce-order:status', 'op' => '==', 'value' => 'checkout_complete' ])
    ->condition(rules_condition('data_is', [ 'data:select' => 'commerce-order-unchanged:status', 'op' => '==', 'value' => 'checkout_complete' ])->negate())
    ->action( "easy_ezcount_action_send_invoice", [ 'order:select' => 'commerce-order'] );


  $order = rules_reaction_rule();
  $order->label =  "Submit Invoice-Receipt to EZCount";
  $order->active = true;
  $order
    ->event('commerce_order_update')
    ->condition('data_is', [ 'data:select' => 'commerce-order:status', 'op' => '==', 'value' => 'completed' ])
    ->condition(rules_condition('data_is', [ 'data:select' => 'commerce-order-unchanged:status', 'op' => '==', 'value' => 'completed' ])->negate())
    ->action( "easy_ezcount_action_send_order", [ 'order:select' => 'commerce-order' ] );

  $memoin = rules_reaction_rule();
  $memoin->label = 'Submit memoin to EZCount';
  $memoin->active = TRUE;
  $memoin
    ->event('node_update')
    ->condition('data_is', array('data:select' => 'node-unchanged:status', 'value' => 0))
    ->condition('data_is', array('data:select' => 'node:status', 'value' => 1))
    ->condition('data_is', array('data:select' => 'node:type', 'value' => 'memoin'))
    ->action('easy_ezcount_action_send_memoin', array('memoin:select' => 'node'));

  $memoout = rules_reaction_rule();
  $memoout->label = 'Submit memoout to EZCount';
  $memoout->active = TRUE;
  $memoout
    ->event('node_update')
    ->condition('data_is', array('data:select' => 'node-unchanged:status', 'value' => 0))
    ->condition('data_is', array('data:select' => 'node:status', 'value' => 1))
    ->condition('data_is', array('data:select' => 'node:type', 'value' => 'memoout'))
    ->action('easy_ezcount_action_send_memoout', array('memoout:select' => 'node'));

  return [
    "easy_ezcount_send_invoice" => $invoice,
    "easy_ezcount_send_order"   => $order,
    "easy_ezcount_send_memoout" => $memoout,
    "easy_ezcount_send_memoin"  => $memoin
  ];
}

