<?php

/**
 * Implements hook_default_rules_configuration().
 */
function _easy_email_default_rules_configuration() {
  // Set order status : Canceled
  $roles = array();
  $rules["rules_easy_email_unsubscribe_subscribe_redirect"] = entity_import('rules_config', '{ "rules_easy_email_unsubscribe_subscribe_redirect" : {
    "LABEL" : "Unsubscribe all , subscribe selected, redirect",
    "PLUGIN" : "action set",
    "OWNER" : "rules",
    "TAGS" : [ "Custom" ],
    "REQUIRES" : [ "easy_email", "simplenews_rules", "rules" ],
    "USES VARIABLES" : { "user" : { "label" : "User", "type" : "user" } },
    "ACTION SET" : [
      { "easy_email_rules_action_unsubscribe_all" : { "tid" : "5" } },
      { "simplenews_rules_action_subscribe" : { "mail" : [ "user:mail" ], "tid" : "5", "confirmation" : "2" } },
      { "redirect" : { "url" : "admin\/content\/newsletter-creation" } }
    ]
  }
}');
  return $rules;
}


