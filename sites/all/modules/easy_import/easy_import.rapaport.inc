<?php


function importRapaport($csvFile){
	
	// Convert the csv to an array object
	// Open the File.
    if (($handle = fopen("sites/default/files/stock/" . $csvFile->filename, "r")) !== FALSE) {
    	// Set the parent multidimensional array key to 0.
        $nn = 0;
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            // Count the total keys in the row.
            $c = count($data);
            // Populate the multidimensional array.
            for ($x=0;$x<$c;$x++) {
                
            	$csv[$nn][$x] = $data[$x];
            }
            $nn++;
        }
        // Close the File.
        fclose($handle);
    }
    
    //what shape are we dealing with ?
    $shape = $csv[0][0];
    
    //delete current records of the shape specified.
    $result = db_query("DELETE FROM {rapaport} WHERE shape='%s'", $shape);

	// And insert new ones.
	for ($i=0; $i<count($csv); $i++) {
    		$result = db_query("INSERT INTO {rapaport} (shape,clarity,color,from_carat,to_carat,price) values ('%s','%s','%s','%s','%s','%s')",$csv[$i][0],$csv[$i][1],$csv[$i][2],$csv[$i][3],$csv[$i][4],$csv[$i][5]);	
    }

	drupal_set_message('Import Completed Successfully');
}