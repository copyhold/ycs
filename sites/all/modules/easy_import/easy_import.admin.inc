<?php

// $Id$



/**

* @file

* Contains forms and form handler functions that make up the batch demo administration section.

*/





/**

* Use Drupal's Form API (FAPI) to wire up buttons to kick off batch operations.

*/

function easy_import_form(&$form_state) {



	$form['#attributes']['enctype'] = 'multipart/form-data';

	$form['file'] = array(

		'#title' 		=> t('Browse for file'),

		'#type'			=> 'file',

		'#description'	=> t('Choose the newsest stock .csv file to upload.'),

	);

	

	$form['submit'] = array(

			'#type'			=> 'submit',

			'#value'		=> t('Submit'),

		);

	

	return $form;

}

/**
 * Submit handler for easy_import_form();
 */
function easy_import_form_submit($form, &$form_state) {
  $limits = array('extensions' => array(1 => 'csv'));
  $validators = array(
    'file_validate_extensions' => $limits['extensions'],
  );

  $option = $form['file']['#post']['type'];
  if ($file = file_save_upload('file', $validators, 'sites/default/files/reports', FILE_EXISTS_REPLACE)) {
    if (basename($file->filepath)!='davidoffrap.csv' && !copy($file->filepath,  dirname($file->filepath) . '/davidoffrap.csv')) {
      drupal_set_message(t('Error while copying stock file. Please contact administrator.'),'error');
      return;
    }
    shell_exec('/usr/local/bin/php ' . $_SERVER['DOCUMENT_ROOT'] . '/importscript.php>/dev/null &2>/dev/null &');
    drupal_set_message(t('Stock import process has been started. Once it will be finished, you will be notified'));
    return;
  } else {
    drupal_set_message('File Problem', 'error');
  }
}

?>