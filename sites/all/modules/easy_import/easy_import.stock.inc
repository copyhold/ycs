<?php
/**

 * Batch setup, This is called from the form submit.

 */

function easy_import_import_stock($csvFile, $isAuto=FALSE) {

  // Update the user profiles to add values to newly added profile fields

  $batch = array(

    'title' => t('Importing diamonds, Please hold'), // Title to display while running.

    'operations' => array(), // Operations to complete, in order. Defined below.

    'finished' => 'stock_batch_import_finished', // Last function to call.

    'init_message' => t('Initializing...'),

    'progress_message' => t('Import is now in progress.'),

    'error_message' => t('Import encountered an error.'),

  );

  //	if($isAuto) copy("stock/" . $csvFile->filename, "sites/default/files/rapaport/" . $csvFile->filename);

  	

  	if (($handle = fopen("sites/default/files/reports/" . $csvFile->filename, "r")) !== FALSE) {

  		$keys[] = getKeys($csvFile);

  		$items[] = getRows($csvFile);

  		fclose($handle);

  		$batch['operations'][] = array('stock_batch_import', array($keys[0], $items[0]));

  		

  	}

  //go!

  batch_set($batch);

  if($isAuto) batch_process('node/1');

}



/**

 * batch operation function as defined in the $batch array.

 */

function stock_batch_import($keys, $items, &$context) {

  $limit=20;



  if (!isset($context['sandbox']['progress'])) {

  	$context['sandbox']['progress'] = 0;

    $context['sandbox']['max'] = count($items);

  } 

   

  if(!isset($context['sandbox']['items'])) {

    $context['sandbox']['items'] = $items;

  }

  

  //begin saving xml feed item as node.

  $counter = 0;   

  if(!empty($context['sandbox']['items'])) {

    if ($context['sandbox']['progress'] != 0){

      array_splice($context['sandbox']['items'], 0, $limit);

    }

    foreach($context['sandbox']['items'] as $item_id => $item) {

      if ($counter != $limit) {

      	//make sure all nessecary fields are available, else skip item and log error

      	if ($item[$keys['shape']] && $item[$keys['carat']] && $item[$keys['ppc']]){

      		if($nid = nodeExists($item[$keys['model']])) //node already exists, just update it

				updateNode($nid, $item, $keys);

			else //new node, create it

				createNode($item,$keys);

			//mark model to be NOT deleted.

			$context['results']['modelsToSave'][] = $item[$keys['model']];

      	}

      	else{

      		 drupal_set_message('The item ' . $item[$keys['model']] . ' in line ' . ($item_id+1) . ' is missing some mandatory fields and could not be imported.', 'error');

  		}

  		

      	$counter++;

  		$context['sandbox']['progress']++;

  		$context['message'] = t('Now processing node %node of %count', array('%node' => $context['sandbox']['progress'], '%count' => $context['sandbox']['max']));

  		$context['results']['nodes'] = $context['sandbox']['progress'];

  	  }

  	 }

  	}

  	if ($context['sandbox']['progress'] != $context['sandbox']['max']) {

  		$context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];

	}

  }



/**

* Batch 'finished' callback

*/

function stock_batch_import_finished($success, $results, $operations) {

	if ($success) {

		// Here we do something meaningful with the results.

	    $message = t('%nodes nodes processed', array('%nodes' => $results['nodes']));

	    watchdog('easy_import', '%nodes nodes processed', array('%nodes' => $results['nodes']), WATCHDOG_NOTICE);

  	} else {

	    // An error occurred.

	    // $operations contains the operations that remained unprocessed.

	    $error_operation = reset($operations);

	    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));

	    watchdog('easy_import', 'An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)), WATCHDOG_ERROR);

	}

	if($results['modelsToSave'])

		deleteNodes($results['modelsToSave']);

	drupal_set_message($message);

}



/**

 * 	@param $data - array with details of one node

 *	@param $keys - array with verbal keys and numerical values, defines the correct keys for the $data array above .

 *	creates a diamond node, and a file object for the certificate image .

 */

function createNode($data,$keys){

	//begin creation of node and basic data

	$node = new StdClass();

	$node->type = 'white_diamond';

	$node->status=1;

	$node->title=$data[$keys['model']];

	node_save($node); //save node with it's type to auto create fields


	//if diamond is a fancy color :

	if ($data[$keys['fancyColor']]){
	
	    $node->taxonomy = array(2 => taxonomy_get_term(2));

		$node->field_wd_fancycolor[0]['value'] = getFancyColorName($data[$keys['fancyColor']]);

	//diamond is white :

	} else {
 
        $node->taxonomy = array(1 => taxonomy_get_term(1));

		$node->field_wd_color[0]['value'] = $data[$keys['color']];

	}

	//data for both fancy and white

	$node->model = $data[$keys['model']];

	$node->field_wd_shape[0]['value'] = getShapeName($data[$keys['shape']]);

	$node->field_wd_clarity[0]['value'] = $data[$keys['clarity']];

	$node->field_wd_weight[0]['value'] = $data[$keys['carat']];

	$node->field_wd_cut[0]['value'] = $data[$keys['cut']];

	$node->field_wd_fluor_int[0]['value'] = getFluoIntensityName($data[$keys['fluorIntensity']]);

	$node->field_wd_symmetry[0]['value'] = $data[$keys['symmetry']];

	$node->field_wd_polish[0]['value'] = $data[$keys['polish']];

	$node->ield_wd_fluor_color[0]['value'] = $data[$keys['fluorColor']];

	$node->	field_wd_culet_size[0]['value'] = $data[$keys['culetSize']];

	$node->field_wd_lab[0]['value'] = $data[$keys['lab']];

	$node->field_wd_certnum[0]['value'] = $data[$keys['certificateNumber']];
	
	$node->field_wd_girdlethick[0]['value'] = $data[$keys['girdleFrom']];

	$node->field_wd_girdlethin[0]['value'] = $data[$keys['girdleTo']];

	$node->field_wd_depth[0]['value'] = $data[$keys['depth']];
	
	$node->field_wd_table[0]['value'] = $data[$keys['table']];

    $node->field_wd_certpath[0]['value']  = $data[$keys['certificateImage']];

	//measurements :

	$measurmentsStr = strtolower(str_replace('-','x',$data[$keys['measurments']]));

	$measurmentsArray = explode('x',$measurmentsStr);

	$node->field_wd_lenght[0]['value'] = $measurmentsArray[0]; 

	$node->field_wd_width[0]['value'] = $measurmentsArray[1];

	$node->field_wd_depthmeas[0]['value'] = $measurmentsArray[2]; 
       
	//price stuff :

	$ppc = intval($data[$keys['ppc']]);

	$node->field_wd_ppc[0]['value'] = $ppc;

	$node->sell_price = $ppc*$data[$keys['carat']];

	//image shit :

	if($data[$keys['certificateImage']]){

			$mime = 'image/jpeg'; 

			$file_drupal_path =  "/sites/default/files/reports/" . $data[$keys['certificateImage']];

			$file = new stdClass();

			$file->filename = basename($file_drupal_path);

			$file->filepath = $file_drupal_path;

			$file->filemime = $mime;

			$file->filesize = filesize($file_drupal_path);

			$file->uid = $uid;

			$file->status = FILE_STATUS_PERMANENT;

			$file->timestamp = time();

			drupal_write_record('files', $file);

			$node->field_wd_cert = array(

				array(

					'fid' => $file->fid,

			        'title' => basename($file->filename),

			        'filename' => $file->filename,

			        'filepath' => $file->filepath,

			        'filesize' => $file->filesize,

			        'mimetype' => $mime,

  			        'description' => basename($file->filename),

			        'list' => 1,

			    ),

			);			

	}	
	
	if($data[$keys['diamondImage']]){

			$mime = 'image/jpeg'; 

			$file_drupal_path =  "sites/default/files/reports/images/" . $data[$keys['diamondImage']];

			$file = new stdClass();

			$file->filename = basename($file_drupal_path);

			$file->filepath = $file_drupal_path;

			$file->filemime = $mime;

			$file->filesize = filesize($file_drupal_path);

			$file->uid = $uid;

			$file->status = FILE_STATUS_PERMANENT;

			$file->timestamp = time();

			drupal_write_record('files', $file);

			$node->field_image_cache = array(

				array(

					'fid' => $file->fid,

			        'title' => basename($file->filename),

			        'filename' => $file->filename,

			        'filepath' => $file->filepath,

			        'filesize' => $file->filesize,

			        'mimetype' => $mime,

  			        'description' => basename($file->filename),

			        'list' => 1,

			    ),

			);			

	}


	//close and save the node

	node_save($node);
	
	return $node;

}



/**

 *	@param $nid - integer, nid of a node.

 * 	@param $data - array with details of one node

 *	@param $keys - array with verbal keys and numerical values, defines the correct keys for the $data array above .

 *	in case of node update, this function loads the existing node and updates it fields.

 */

function updateNode($nid, $data, $keys){

	$node = node_load($nid);

	//$node->sell_price = $data[$keys['sellPrice']];

	$node->model = $data[$keys['model']];

	$node->field_wd_shape[0]['value'] = getShapeName($data[$keys['shape']]);

	$node->field_wd_clarity[0]['value'] = $data[$keys['clarity']];

	$node->field_wd_weight[0]['value'] = $data[$keys['carat']];

	$node->field_wd_cut[0]['value'] = $data[$keys['cut']];

	$node->field_wd_fluor_int[0]['value'] = getFluoIntensityName($data[$keys['fluorIntensity']]);

	$node->field_wd_symmetry[0]['value'] = $data[$keys['symmetry']];

	$node->field_wd_polish[0]['value'] = $data[$keys['polish']];

	$node->ield_wd_fluor_color[0]['value'] = $data[$keys['fluorColor']];

	$node->	field_wd_culet_size[0]['value'] = $data[$keys['culetSize']];

	$node->field_wd_lab[0]['value'] = $data[$keys['lab']];

	$node->field_wd_certnum[0]['value'] = $data[$keys['certificateNumber']];
	
	$node->field_wd_girdlethick[0]['value'] = getGirdleName($data[$keys['girdleFrom']]);

	$node->field_wd_girdlethin[0]['value'] = getGirdleName($data[$keys['girdleTo']]);

	$node->field_wd_depth[0]['value'] = $data[$keys['depth']];
	
	$node->field_wd_table[0]['value'] = $data[$keys['table']];

    $node->field_wd_certpath[0]['value']  = $data[$keys['certificateImage']];

	//measurements :

	$measurmentsStr = strtolower(str_replace('-','x',$data[$keys['measurments']]));

	$measurmentsArray = explode('x',$measurmentsStr);

	$node->field_wd_lenght[0]['value'] = $measurmentsArray[0]; 

	$node->field_wd_width[0]['value'] = $measurmentsArray[1];

	$node->field_wd_depthmeas[0]['value'] = $measurmentsArray[2]; 

	//price stuff :

	$ppc = intval($data[$keys['ppc']]);

	$node->field_wd_ppc[0]['value'] = $ppc;

	$node->sell_price = $ppc*$data[$keys['carat']];
	
	
	if($data[$keys['diamondImage']] && $node->field_image_cache[0]['filepath']==''){

			$mime = 'image/jpeg'; 

			$file_drupal_path =  "sites/default/files/reports/images/" . $data[$keys['diamondImage']];

			$file = new stdClass();

			$file->filename = basename($file_drupal_path);

			$file->filepath = $file_drupal_path;

			$file->filemime = $mime;

			$file->filesize = filesize($file_drupal_path);

			$file->uid = $uid;

			$file->status = FILE_STATUS_PERMANENT;

			$file->timestamp = time();

			drupal_write_record('files', $file);

			$node->field_image_cache = array(

				array(

					'fid' => $file->fid,

			        'title' => basename($file->filename),

			        'filename' => $file->filename,

			        'filepath' => $file->filepath,

			        'filesize' => $file->filesize,

			        'mimetype' => $mime,

  			        'description' => basename($file->filename),

			        'list' => 1,

			    ),

			);			

	}

	
	
	$node->status = 1;

	node_save($node);

}

/**

 * unpublishes old nodes that are not present in the current stock csv file .

 */

function deleteNodes($modelsToSave){

	$str = 'n.title NOT LIKE "'.implode('" AND n.title NOT LIKE "',$modelsToSave);

	

	$query = sprintf('SELECT n.nid FROM {node} n

					  WHERE (n.type="white_diamond") AND (n.status=1) AND %s' , $str).'"';

	variable_set('foo',$query);

	if($result = db_query($query)){

		$nids = array();

		while($nid = db_fetch_array($result)){

			$nids[] = $nid;

		}

		foreach($nids as $theNid){

			$result = db_query('UPDATE {node} SET status=0 WHERE nid=%d',$theNid['nid']);

			drupal_set_message('The diamond ' . $nid['model'] . ' has been unpublished.');

		}

	}

}

	

/**

 * 	@param $model - a string variable of a node model.

 *	checks by model if a node already exist

 */

function nodeExists($model){

	$result = db_result(db_query('select p.nid from {uc_products} p where p.model="%s"',$model));

	return $result;

}



/**

 * Looks for correct keys in the csvFile

 */

function getKeys($csvLine) {

     $array = array();

     $data = str_getcsv($csvLine);
     
	 $num = count($data);

	    	for ($c=0; $c < $num; $c++) {

	    		//looking for keys, add more keywords here using cases.

    			switch($data[$c]) {

            		// id key

            		case 'Supplier Stock Ref' :

	            	case 'stock #' :

	            	case 'VendorStockNumber' :

	            		$array['model'] = $c; ; break;

	            		break;

	            	//Sell Price

	            		case 'Price' :

	            			$array['ppc'] = $c; break;

	            	//color

	            	case 'Color' :

	            		$array['color'] = $c; break;

	            	//cut(Shape)

	            	case 'Cut (Shape)' :

	            	case 'shape' :

	            	case 'Shape' :

	            		$array['shape'] = $c; break;

	            	//clarity

	            	case 'Clarity' :

	            		$array['clarity'] = $c; break;

	            	//carat (weight)

	            	case 'Weight' :

	            		$array['carat'] = $c; break;

	            	//measurments(length, width, height)

	            	case 'Measurements' :

	            		$array['measurments'] = $c; break;

	            	//cut(grade)

	            	case 'Cut Grade' :

	            		$array['cut'] = $c; break;

	            	//Fluorescence Intensity

	            	case 'Fluorescence Intensity' :

	            		$array['fluorIntensity']= $c; break;

	            	//Table Percentage

	            	case 'Table %' :

	            		$array['table'] = $c; break;

	            	//Symmetry

	            	case 'Symmetry' :

	            		$array['symmetry'] = $c; break;

	            	//Polish

	            	case 'Polish' :

	            		$array['polish'] = $c; break;

	            	//Fluorescence Color

	            	case 'Fluorescence Color' :

	            		$array['fluorColor'] = $c; break;

	            	//Treatment

	            	case 'Treatment' :

	            		$array['treatment'] = $c; break;

	            	//Culet Condition

	            	case 'Culet Condition' :

	            		$array['cultcond'] = $c; break;

	            	//Girdle thickness(from)

	            	case 'Girdle Thin' :

	            		$array['girdleFrom'] = $c; break;

	            	//Girdle thickness(to)

	            	case 'Girdle Thick' :

	            		$array['girdleTo'] = $c; break;

	            	//Culet Size

	            	case 'Culet Size' :

	            		$array['culetSize'] = $c; break;

	            	//Laboratory

	            	case 'Lab' :

	            		$array['lab'] = $c; break;

	            	//Certificate Image

	            	case 'Certificate Image' :

	            		$array['certificateImage'] = $c; break;

	            	//Certificate Number

	            	case 'Certificate #' :

	            		$array['certificateNumber'] = $c; break;

	            	//Laser inscription

	            	case 'LaserInscription' :

	            		$array['laserInscription'] = $c; break;

	            	//Depth(percenatge)

	            	case 'Depth %' :

	            		$array['depth'] = $c; break;

	              	//fancy color

	            	case 'FancyColor' :

	            		$array['fancyColor'] = $c; break;

	            	//Fancy Color Intensity

	            	case 'FancyColorIntensity' :

	            		$array['fancyColorIntensity'] = $c; break;

	            	//FancyColorOvertone

					case 'FancyColorOvertone' :

						$array['fancyColorOvertone'] = $c; break;
						
					case 'Diamond Image' :
					
					   $array['diamondImage'] = $c; break;

            	}

            }

    return $array;
}

/**
 * returns multidimensial array filled with data from the csv file uploaded by user.
 */

function getRows($csvFile){

    $nn = 0;

    if (($handle = fopen("sites/default/files/reports/" . $csvFile->filename, "r")) !== FALSE) {

	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

	    	// Count the total keys in the row.

	        $c = count($data);

	        // Populate the multidimensional array.

	        for ($x=0;$x<$c;$x++) {

	        	$csv[$nn][$x] = $data[$x];

	        }

	        $nn++;

	    }

    }

    fclose($handle);

    array_shift($csv);

    return $csv;

}



/**

 * returns a price specified by rapaport

 */

function getRapPrice($shape,$color,$clarity,$carat){

	$query = "select r.price from {rapaport} r where r.shape='%s' and r.color='%s' and r.clarity='%s' and %s between r.from_carat and r.to_carat";

	$result = db_query($query, 'PS', $color,$clarity,$carat);

	if($result)

		$price = db_fetch_object($result);

	

	return $price->price;

}



/**

 * returns easystock valid shape name

 */

function getShapeName($data){

	$round = array('B', 'BR', 'RB', 'RD', 'RBC', 'Round', 'Brilliant', 'RND');

	$pear = array('P', 'PS', 'PSH');

	$emerald = array('E', 'EM','EC');

	$princess = array('PRN', 'PR', 'PRIN', 'PN', 'PC', 'MDSQB', 'SMB');

	$marquise = array('MQB', 'M', 'MQ');

	$asscher = array('A', 'CSS', 'AC', 'AS');

	$cushion = array('C', 'CUX','CU', 'CB', 'CUSH', 'CUS');

	$heart = array('H', 'HS', 'HT');

	$oval = array('O', 'OV');

	$radiant = array('R', 'RAD', 'RA', 'RC', 'RDN');

	$triangle = array('Triangular', 'TRI','TR');

	if(in_array($data,$round)) return 'Round';

	if(in_array($data,$pear)) return 'Pear';

	if(in_array($data,$emerald)) return 'Emerald';

	if(in_array($data,$princess)) return 'Princess';

	if(in_array($data,$marquise)) return 'Marquise';

	if(in_array($data,$asscher)) return 'Asscher';

	if(in_array($data,$cushion)) return 'Cushion';

	if(in_array($data,$heart)) return 'Heart';

	if(in_array($data,$oval)) return 'Oval';

	if(in_array($data,$radiant)) return 'Radiant';

	if(in_array($data,$triangle)) return 'Triangle';

	return 'Special';

}



/**

 * returns easystock valid polish name

 */

function getPolishName($data){

	switch($data){

		case 'G' : return 'Good';

		case 'VG' : return 'Very Good';

		case 'X' : return 'Excellent';

	}

}



/**

 * return easystock valid cut name

 */

function getCutName($data){

	switch($data){

		case 'VG' : return 'Very Good';

	}

}



/**

 * returns easystock valid color intensity name

 */

function getColorIntensityName($data){

	switch($data){

		case 'F' : return 'Faint';

		case 'VL' : return 'Very Light';

		case 'L' : return 'Light';

		case 'FCL' : return 'Fancy Light';

		case 'FC' : return 'Fancy';

		case 'FCD' : return 'Fancy Dark';

		case 'I' : return 'Fancy Intense';

		case 'FV' : return 'Fancy Vivid';

		case 'D' : return 'Fancy Deep';

	}

}



/**

 * returns easystock valid symmetery name

 */

function getSymmetryName($data){

	switch($data){

		case 'I' : return 'Ideal';

		case 'EX' :

		case 'X' : return 'Excellent';

		case 'VG' : return 'Very Good';

		case 'G' : return 'Good';

		case 'F' : return 'Fair';

	}

}



/**

 * returns easystock valid fluorosence intensity name

 */

function getFluoIntensityName($data){

	switch($data){

		case 'F' : return 'Faint';

		case 'N' : return 'None';

		case 'M' : return 'Medium';

		case 'S' : return 'Strong';

	}

}



function getFancyColorName($data){

	switch($data){

		case 'BK': return 'Black';

		case 'B' : return 'Blue';

		case 'BN': return 'Brown';

		case 'CM': return 'Champagne';

		case 'CG': return 'Cognac';

		case 'GY': return 'Gray';

		case 'G' : return 'Green';

		case 'O' : return 'Orange';

		case 'P' : return 'Pink';

		case 'PL': return 'Purple';

		case 'R' : return 'Red';

		case 'V' : return 'Violet';

		case 'Y' : return 'Yellow';

		case 'W' : return 'White';

		case 'X' : return 'Other';

	}	

}



/**

 * returns easystock valid culet size name

 */

function getCuletSizeName($data){

	switch($data) {

		case 'VL' : return 'Very Large';

		case 'L' : return 'Large';

		case 'M' : return 'Medium';

		case 'S' : return 'Small';

		case 'VS' : return 'Very Small';

		case 'N' : return 'None';

	}

}



function getGirdleName($data){

	$extremelyThick = array('XTK', 'EXTHICK', 'EXTHK', 'XTHK', 'XTHICK', 'XTHIK', 'ETK', 'EK', 'XK');

	$veryThick		= array('VTK', 'VTHCK', 'VTHK', 'VTHICK');

	$Thick 			= array('TK', 'THK', 'THIK', 'THIC');

	$slightlyThick  = array('STK', 'SLTK', 'SLTHK');

	$medium 		= array('M', 'MED', 'MD');

	$thin 			= array('TN', 'THN');

	$slightlyThin 	= array('STN', 'SLTN', 'SLTHN');

	$veryThin		= array('VTN', 'VTHN', 'VTHIN', 'VN');

	$extremelyThin  = array('XTN', 'XTHN', 'EXTN', 'ETN', 'EN', 'XN');

	

	if(in_array($data,$extremelyThick))	return 'Extremely Thick';

	if(in_array($data,$veryThick))		return 'Very Thick';

	if(in_array($data,$Thick)) 			return 'Thick';

	if(in_array($data,$slightlyThick))	return 'Slightly Thick';

	if(in_array($data,$medium))			return 'Medium';

	if(in_array($data,$thin)) 			return 'Thin';

	if(in_array($data,$slightlyThin)) 	return 'Slightly Thin';

	if(in_array($data,$veryThin)) 		return 'Very Thin';

	if(in_array($data,$extremelyThin)) 	return 'Extremely Thin';

}